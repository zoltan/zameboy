#include <stdarg.h>
#include <stdio.h>

#include "logging.h"


static enum LogLevel _level = LOG_ERROR;
static const char * _level_names[] = {"ERROR", "WARNING", "INFO", "DEBUG"};
static const char * _domains_names[] = {"core", "cpu", "bus"};


void logging_set_level(enum LogLevel level)
{
  if (level >= (sizeof(_level_names) / sizeof(*_level_names))) {
    level = sizeof(_level_names) / sizeof(*_level_names) - 1;
  }
  _level = level;
  log_info(LOG_CORE, "Logging level set to %s", _level_names[level]);
}

void log_message(enum LogLevel level, enum LogDomain domain, const char *fmt, va_list ap)
{
  char buffer[1024];

  if (level <= _level) {
    snprintf(buffer, 1024, "%s:%s: %s\n", _level_names[level], _domains_names[domain], fmt);
    vprintf(buffer, ap);
  }
}

void log_error(enum LogDomain domain, const char *fmt, ...)
{
  va_list ap;

  va_start(ap, fmt);
  log_message(LOG_ERROR, domain, fmt, ap);
  va_end(ap);
}

void log_warning(enum LogDomain domain, const char *fmt, ...)
{
  va_list ap;

  va_start(ap, fmt);
  log_message(LOG_WARNING, domain, fmt, ap);
  va_end(ap);
}

void log_info(enum LogDomain domain, const char *fmt, ...)
{
  va_list ap;

  va_start(ap, fmt);
  log_message(LOG_INFO, domain, fmt, ap);
  va_end(ap);
}

void log_debug(enum LogDomain domain, const char *fmt, ...)
{
  va_list ap;

  va_start(ap, fmt);
  log_message(LOG_DEBUG, domain, fmt, ap);
  va_end(ap);
}
