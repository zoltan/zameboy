#ifndef CPU_H
#define CPU_H

#include "zameboy.h"

u_int64_t run(u_int64_t cycles_available, struct zameboy * zameboy);

#endif /* CPU_H */
