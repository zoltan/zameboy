#include "bus.h"
#include "instruction.h"
#include "zameboy.h"

u_int8_t jr_r8(struct zb_bus * bus, struct zb_registers * regs)
{
  REG_PC(regs) += bus_read8_signed(bus, REG_PC(regs) + 1);
  return 12;
}

u_int8_t jr_c_r8(struct zb_bus * bus, struct zb_registers * regs)   { return (REG_F(regs) & ZB_FLAG_C) ? 8 : jr_r8(bus, regs); }
u_int8_t jr_z_r8(struct zb_bus * bus, struct zb_registers * regs)   { return (REG_F(regs) & ZB_FLAG_Z) ? 8 : jr_r8(bus, regs); }
u_int8_t jr_nc_r8(struct zb_bus * bus, struct zb_registers * regs)  { return (!(REG_F(regs) & ZB_FLAG_C)) ? 8 : jr_r8(bus, regs); }
u_int8_t jr_nz_r8(struct zb_bus * bus, struct zb_registers * regs)  { return (!(REG_F(regs) & ZB_FLAG_Z)) ? 8 : jr_r8(bus, regs); }


u_int8_t jp_a16(struct zb_bus * bus, struct zb_registers * regs)
{
  REG_PC(regs) = bus_read16(bus, REG_PC(regs) + 1);
  return 16;
}

u_int8_t jp_c_a16(struct zb_bus * bus, struct zb_registers * regs)   { return (REG_F(regs) & ZB_FLAG_C) ? 12 : jp_a16(bus, regs); }
u_int8_t jp_z_a16(struct zb_bus * bus, struct zb_registers * regs)   { return (REG_F(regs) & ZB_FLAG_Z) ? 12 : jp_a16(bus, regs); }
u_int8_t jp_nc_a16(struct zb_bus * bus, struct zb_registers * regs)  { return (!(REG_F(regs) & ZB_FLAG_C)) ? 12 : jp_a16(bus, regs); }
u_int8_t jp_nz_a16(struct zb_bus * bus, struct zb_registers * regs)  { return (!(REG_F(regs) & ZB_FLAG_Z)) ? 12 : jp_a16(bus, regs); }
u_int8_t jp_hl(struct zb_bus * bus, struct zb_registers * regs)
{
  REG_PC(regs) = bus_read16(bus, REG_HL(regs));
  return 16;
}

u_int8_t call(struct zb_bus * bus, struct zb_registers * regs)
{
  REG_SP(regs) -= 2;
  // CALL pushes the address for pc after RET, thus the current instruction's
  // size must be added
  bus_write16(bus, REG_SP(regs), REG_PC(regs) + 3);
  // the cpu loop adds te instruction size (3) to PC no matter what, so we
  // cancel that by removing the size from PC.
  REG_PC(regs) = bus_read16(bus, REG_PC(regs) + 1) - 3;
  return 24;
}

u_int8_t call_c(struct zb_bus * bus, struct zb_registers * regs)    { return (REG_F(regs) & ZB_FLAG_C) ? 12 : call(bus, regs); }
u_int8_t call_z(struct zb_bus * bus, struct zb_registers * regs)    { return (REG_F(regs) & ZB_FLAG_Z) ? 12 : call(bus, regs); }
u_int8_t call_nc(struct zb_bus * bus, struct zb_registers * regs)   { return (!(REG_F(regs) & ZB_FLAG_C)) ? 12 : call(bus, regs); }
u_int8_t call_nz(struct zb_bus * bus, struct zb_registers * regs)   { return (!(REG_F(regs) & ZB_FLAG_Z)) ? 12 : call(bus, regs); }

u_int8_t ret(struct zb_bus * bus, struct zb_registers * regs)
{
  // RET sets PC to the next instruction addr, but the cpu loop will add RET
  // instruction size (1) no matter what, so we have to "cancel" that by
  // removing the same value from PC.
  REG_PC(regs) = bus_read16(bus, REG_SP(regs)) - 1;
  REG_SP(regs) += 2;
  return 16;
}

u_int8_t ret_c(struct zb_bus * bus, struct zb_registers * regs)    { return (REG_F(regs) & ZB_FLAG_C) ? 8 : ret(bus, regs) + 4; }
u_int8_t ret_z(struct zb_bus * bus, struct zb_registers * regs)    { return (REG_F(regs) & ZB_FLAG_Z) ? 8 : ret(bus, regs) + 4; }
u_int8_t ret_nc(struct zb_bus * bus, struct zb_registers * regs)   { return (!(REG_F(regs) & ZB_FLAG_C)) ? 8 : ret(bus, regs) + 4; }
u_int8_t ret_nz(struct zb_bus * bus, struct zb_registers * regs)   { return (!(REG_F(regs) & ZB_FLAG_Z)) ? 8 : ret(bus, regs) + 4; }
u_int8_t reti(struct zb_bus * bus, struct zb_registers * regs)
{
  bus->ime = true;
  return ret(bus, regs);
}


#define func_rst(addr)                              \
  REG_SP(regs) -= 2;                                \
  bus_write16(bus, REG_SP(regs), REG_PC(regs) + 1); \
  REG_PC(regs) = addr;                              \
  return 16;

u_int8_t rst_00h(struct zb_bus * bus, struct zb_registers * regs) { func_rst(0x00) }
u_int8_t rst_08h(struct zb_bus * bus, struct zb_registers * regs) { func_rst(0x08) }
u_int8_t rst_10h(struct zb_bus * bus, struct zb_registers * regs) { func_rst(0x10) }
u_int8_t rst_18h(struct zb_bus * bus, struct zb_registers * regs) { func_rst(0x18) }
u_int8_t rst_20h(struct zb_bus * bus, struct zb_registers * regs) { func_rst(0x20) }
u_int8_t rst_28h(struct zb_bus * bus, struct zb_registers * regs) { func_rst(0x28) }
u_int8_t rst_30h(struct zb_bus * bus, struct zb_registers * regs) { func_rst(0x30) }
u_int8_t rst_38h(struct zb_bus * bus, struct zb_registers * regs) { func_rst(0x38) }
