#include "bus.h"
#include "instruction.h"
#include "zameboy.h"


#define func_bit_test(rname, offset) \
  REG_F(regs) = 0 | ZB_FLAG_H | (REG_F(regs) & ZB_FLAG_C) | ((rname(regs) & (1 << offset)) ? ZB_FLAG_Z : 0); \
  return 8;

#define func_bit_test_hl(offset) \
  zb_byte value;\
\
  value = bus_read8(bus, REG_HL(regs));\
  REG_F(regs) = 0 | ZB_FLAG_H | (REG_F(regs) & ZB_FLAG_C) | ((value & (1 << offset)) ? ZB_FLAG_Z : 0); \
  return 16;


u_int8_t bit0_a(struct zb_bus * bus, struct zb_registers * regs) { func_bit_test(REG_A, 0) }
u_int8_t bit0_b(struct zb_bus * bus, struct zb_registers * regs) { func_bit_test(REG_B, 0) }
u_int8_t bit0_c(struct zb_bus * bus, struct zb_registers * regs) { func_bit_test(REG_C, 0) }
u_int8_t bit0_d(struct zb_bus * bus, struct zb_registers * regs) { func_bit_test(REG_D, 0) }
u_int8_t bit0_e(struct zb_bus * bus, struct zb_registers * regs) { func_bit_test(REG_E, 0) }
u_int8_t bit0_h(struct zb_bus * bus, struct zb_registers * regs) { func_bit_test(REG_H, 0) }
u_int8_t bit0_l(struct zb_bus * bus, struct zb_registers * regs) { func_bit_test(REG_L, 0) }

u_int8_t bit1_a(struct zb_bus * bus, struct zb_registers * regs) { func_bit_test(REG_A, 1) }
u_int8_t bit1_b(struct zb_bus * bus, struct zb_registers * regs) { func_bit_test(REG_B, 1) }
u_int8_t bit1_c(struct zb_bus * bus, struct zb_registers * regs) { func_bit_test(REG_C, 1) }
u_int8_t bit1_d(struct zb_bus * bus, struct zb_registers * regs) { func_bit_test(REG_D, 1) }
u_int8_t bit1_e(struct zb_bus * bus, struct zb_registers * regs) { func_bit_test(REG_E, 1) }
u_int8_t bit1_h(struct zb_bus * bus, struct zb_registers * regs) { func_bit_test(REG_H, 1) }
u_int8_t bit1_l(struct zb_bus * bus, struct zb_registers * regs) { func_bit_test(REG_L, 1) }

u_int8_t bit2_a(struct zb_bus * bus, struct zb_registers * regs) { func_bit_test(REG_A, 2) }
u_int8_t bit2_b(struct zb_bus * bus, struct zb_registers * regs) { func_bit_test(REG_B, 2) }
u_int8_t bit2_c(struct zb_bus * bus, struct zb_registers * regs) { func_bit_test(REG_C, 2) }
u_int8_t bit2_d(struct zb_bus * bus, struct zb_registers * regs) { func_bit_test(REG_D, 2) }
u_int8_t bit2_e(struct zb_bus * bus, struct zb_registers * regs) { func_bit_test(REG_E, 2) }
u_int8_t bit2_h(struct zb_bus * bus, struct zb_registers * regs) { func_bit_test(REG_H, 2) }
u_int8_t bit2_l(struct zb_bus * bus, struct zb_registers * regs) { func_bit_test(REG_L, 2) }

u_int8_t bit3_a(struct zb_bus * bus, struct zb_registers * regs) { func_bit_test(REG_A, 3) }
u_int8_t bit3_b(struct zb_bus * bus, struct zb_registers * regs) { func_bit_test(REG_B, 3) }
u_int8_t bit3_c(struct zb_bus * bus, struct zb_registers * regs) { func_bit_test(REG_C, 3) }
u_int8_t bit3_d(struct zb_bus * bus, struct zb_registers * regs) { func_bit_test(REG_D, 3) }
u_int8_t bit3_e(struct zb_bus * bus, struct zb_registers * regs) { func_bit_test(REG_E, 3) }
u_int8_t bit3_h(struct zb_bus * bus, struct zb_registers * regs) { func_bit_test(REG_H, 3) }
u_int8_t bit3_l(struct zb_bus * bus, struct zb_registers * regs) { func_bit_test(REG_L, 3) }

u_int8_t bit4_a(struct zb_bus * bus, struct zb_registers * regs) { func_bit_test(REG_A, 4) }
u_int8_t bit4_b(struct zb_bus * bus, struct zb_registers * regs) { func_bit_test(REG_B, 4) }
u_int8_t bit4_c(struct zb_bus * bus, struct zb_registers * regs) { func_bit_test(REG_C, 4) }
u_int8_t bit4_d(struct zb_bus * bus, struct zb_registers * regs) { func_bit_test(REG_D, 4) }
u_int8_t bit4_e(struct zb_bus * bus, struct zb_registers * regs) { func_bit_test(REG_E, 4) }
u_int8_t bit4_h(struct zb_bus * bus, struct zb_registers * regs) { func_bit_test(REG_H, 4) }
u_int8_t bit4_l(struct zb_bus * bus, struct zb_registers * regs) { func_bit_test(REG_L, 4) }

u_int8_t bit5_a(struct zb_bus * bus, struct zb_registers * regs) { func_bit_test(REG_A, 5) }
u_int8_t bit5_b(struct zb_bus * bus, struct zb_registers * regs) { func_bit_test(REG_B, 5) }
u_int8_t bit5_c(struct zb_bus * bus, struct zb_registers * regs) { func_bit_test(REG_C, 5) }
u_int8_t bit5_d(struct zb_bus * bus, struct zb_registers * regs) { func_bit_test(REG_D, 5) }
u_int8_t bit5_e(struct zb_bus * bus, struct zb_registers * regs) { func_bit_test(REG_E, 5) }
u_int8_t bit5_h(struct zb_bus * bus, struct zb_registers * regs) { func_bit_test(REG_H, 5) }
u_int8_t bit5_l(struct zb_bus * bus, struct zb_registers * regs) { func_bit_test(REG_L, 5) }

u_int8_t bit6_a(struct zb_bus * bus, struct zb_registers * regs) { func_bit_test(REG_A, 6) }
u_int8_t bit6_b(struct zb_bus * bus, struct zb_registers * regs) { func_bit_test(REG_B, 6) }
u_int8_t bit6_c(struct zb_bus * bus, struct zb_registers * regs) { func_bit_test(REG_C, 6) }
u_int8_t bit6_d(struct zb_bus * bus, struct zb_registers * regs) { func_bit_test(REG_D, 6) }
u_int8_t bit6_e(struct zb_bus * bus, struct zb_registers * regs) { func_bit_test(REG_E, 6) }
u_int8_t bit6_h(struct zb_bus * bus, struct zb_registers * regs) { func_bit_test(REG_H, 6) }
u_int8_t bit6_l(struct zb_bus * bus, struct zb_registers * regs) { func_bit_test(REG_L, 6) }

u_int8_t bit7_a(struct zb_bus * bus, struct zb_registers * regs) { func_bit_test(REG_A, 7) }
u_int8_t bit7_b(struct zb_bus * bus, struct zb_registers * regs) { func_bit_test(REG_B, 7) }
u_int8_t bit7_c(struct zb_bus * bus, struct zb_registers * regs) { func_bit_test(REG_C, 7) }
u_int8_t bit7_d(struct zb_bus * bus, struct zb_registers * regs) { func_bit_test(REG_D, 7) }
u_int8_t bit7_e(struct zb_bus * bus, struct zb_registers * regs) { func_bit_test(REG_E, 7) }
u_int8_t bit7_h(struct zb_bus * bus, struct zb_registers * regs) { func_bit_test(REG_H, 7) }
u_int8_t bit7_l(struct zb_bus * bus, struct zb_registers * regs) { func_bit_test(REG_L, 7) }

u_int8_t bit0_hl(struct zb_bus * bus, struct zb_registers * regs) { func_bit_test_hl(0) }
u_int8_t bit1_hl(struct zb_bus * bus, struct zb_registers * regs) { func_bit_test_hl(1) }
u_int8_t bit2_hl(struct zb_bus * bus, struct zb_registers * regs) { func_bit_test_hl(2) }
u_int8_t bit3_hl(struct zb_bus * bus, struct zb_registers * regs) { func_bit_test_hl(3) }
u_int8_t bit4_hl(struct zb_bus * bus, struct zb_registers * regs) { func_bit_test_hl(4) }
u_int8_t bit5_hl(struct zb_bus * bus, struct zb_registers * regs) { func_bit_test_hl(5) }
u_int8_t bit6_hl(struct zb_bus * bus, struct zb_registers * regs) { func_bit_test_hl(6) }
u_int8_t bit7_hl(struct zb_bus * bus, struct zb_registers * regs) { func_bit_test_hl(7) }

u_int8_t rra(struct zb_bus * bus, struct zb_registers * regs)
{
  bool bit0 = REG_A(regs) & 0x1;

  REG_A(regs) = REG_A(regs) >> 1 | (HAS_FLAG_C(regs) ? 0x80 : 0);
  REG_F(regs) = bit0 ? ZB_FLAG_C : 0;
  return 4;
}

u_int8_t rrca(struct zb_bus * bus, struct zb_registers * regs)
{
  bool bit0 = REG_A(regs) & 0x1;

  REG_A(regs) = REG_A(regs) >> 1 | bit0 << 7;
  REG_F(regs) = bit0 ? ZB_FLAG_C : 0;
  return 4;
}

u_int8_t rla(struct zb_bus * bus, struct zb_registers * regs)
{
  bool bit7 = REG_A(regs) & 0x1;

  REG_A(regs) = REG_A(regs) << 1 | (HAS_FLAG_C(regs) ? 1 : 0);
  REG_F(regs) = bit7 ? ZB_FLAG_C : 0;
  return 4;
}

u_int8_t rlca(struct zb_bus * bus, struct zb_registers * regs)
{
  bool bit7 = REG_A(regs) & 0x80;

  REG_A(regs) = REG_A(regs) << 1 | bit7 >> 7;
  REG_F(regs) = bit7 ? ZB_FLAG_C : 0;
  return 4;
}

#define func_rlc(rname)                                                 \
  bool bit7 = rname(regs) & 0x80;                                       \
                                                                        \
  rname(regs) = rname(regs) << 1 | bit7 >> 7;                           \
  REG_F(regs) = (bit7 ? ZB_FLAG_C : 0) | (rname(regs) == 0 ? ZB_FLAG_Z : 0); \
  return 8;

u_int8_t rlc_a(struct zb_bus * bus, struct zb_registers * regs) { func_rlc(REG_A) }
u_int8_t rlc_b(struct zb_bus * bus, struct zb_registers * regs) { func_rlc(REG_B) }
u_int8_t rlc_c(struct zb_bus * bus, struct zb_registers * regs) { func_rlc(REG_C) }
u_int8_t rlc_d(struct zb_bus * bus, struct zb_registers * regs) { func_rlc(REG_D) }
u_int8_t rlc_e(struct zb_bus * bus, struct zb_registers * regs) { func_rlc(REG_E) }
u_int8_t rlc_h(struct zb_bus * bus, struct zb_registers * regs) { func_rlc(REG_H) }
u_int8_t rlc_l(struct zb_bus * bus, struct zb_registers * regs) { func_rlc(REG_L) }
u_int8_t rlc_hl(struct zb_bus * bus, struct zb_registers * regs)
{
  zb_byte value = bus_read8(bus, REG_HL(regs));
  bool bit7 = value & 0x80;

  value = value << 1 | bit7 >> 7;
  bus_write8(bus, REG_HL(regs), value);
  REG_F(regs) = (bit7 ? ZB_FLAG_C : 0) | (value == 0 ? ZB_FLAG_Z : 0);
  return 16;
}

#define func_rrc(rname)                                                 \
  bool bit0 = rname(regs) & 0x1;                                        \
                                                                        \
  rname(regs) = rname(regs) >> 1 | bit0 << 7;                           \
  REG_F(regs) = (bit0 ? ZB_FLAG_C : 0) | (rname(regs) == 0 ? ZB_FLAG_Z : 0); \
  return 8;

u_int8_t rrc_a(struct zb_bus * bus, struct zb_registers * regs) { func_rrc(REG_A) }
u_int8_t rrc_b(struct zb_bus * bus, struct zb_registers * regs) { func_rrc(REG_B) }
u_int8_t rrc_c(struct zb_bus * bus, struct zb_registers * regs) { func_rrc(REG_C) }
u_int8_t rrc_d(struct zb_bus * bus, struct zb_registers * regs) { func_rrc(REG_D) }
u_int8_t rrc_e(struct zb_bus * bus, struct zb_registers * regs) { func_rrc(REG_E) }
u_int8_t rrc_h(struct zb_bus * bus, struct zb_registers * regs) { func_rrc(REG_H) }
u_int8_t rrc_l(struct zb_bus * bus, struct zb_registers * regs) { func_rrc(REG_L) }
u_int8_t rrc_hl(struct zb_bus * bus, struct zb_registers * regs)
{
  zb_byte value = bus_read8(bus, REG_HL(regs));
  bool bit0 = value & 0x80;

  value = value >> 1 | bit0 << 7;
  bus_write8(bus, REG_HL(regs), value);
  REG_F(regs) = (bit0 ? ZB_FLAG_C : 0) | (value == 0 ? ZB_FLAG_Z : 0);
  return 16;
}

#define func_rl(rname)                                                  \
  bool bit7 = rname(regs) & 0x80;                                       \
                                                                        \
  rname(regs) = rname(regs) << 1 | (HAS_FLAG_C(regs) ? 1 : 0);          \
  REG_F(regs) = (bit7 ? ZB_FLAG_C : 0) | (rname(regs) == 0 ? ZB_FLAG_Z : 0); \
  return 8;

u_int8_t rl_a(struct zb_bus * bus, struct zb_registers * regs) { func_rl(REG_A) }
u_int8_t rl_b(struct zb_bus * bus, struct zb_registers * regs) { func_rl(REG_B) }
u_int8_t rl_c(struct zb_bus * bus, struct zb_registers * regs) { func_rl(REG_C) }
u_int8_t rl_d(struct zb_bus * bus, struct zb_registers * regs) { func_rl(REG_D) }
u_int8_t rl_e(struct zb_bus * bus, struct zb_registers * regs) { func_rl(REG_E) }
u_int8_t rl_h(struct zb_bus * bus, struct zb_registers * regs) { func_rl(REG_H) }
u_int8_t rl_l(struct zb_bus * bus, struct zb_registers * regs) { func_rl(REG_L) }
u_int8_t rl_hl(struct zb_bus * bus, struct zb_registers * regs)
{
  zb_byte value = bus_read8(bus, REG_HL(regs));
  bool bit7 = value & 0x80;

  value = value << 1 | (HAS_FLAG_C(regs) ? 1 : 0);
  bus_write8(bus, REG_HL(regs), value);
  REG_F(regs) = (bit7 ? ZB_FLAG_C : 0) | (value == 0 ? ZB_FLAG_Z : 0);
  return 16;
}

#define func_rr(rname)                                                  \
  bool bit0 = rname(regs) & 0x1;                                        \
                                                                        \
  rname(regs) = rname(regs) >> 1 | (HAS_FLAG_C(regs) ? 0x80 : 0);       \
  REG_F(regs) = (bit0 ? ZB_FLAG_C : 0) | (rname(regs) == 0 ? ZB_FLAG_Z : 0); \
  return 8;

u_int8_t rr_a(struct zb_bus * bus, struct zb_registers * regs) { func_rr(REG_A) }
u_int8_t rr_b(struct zb_bus * bus, struct zb_registers * regs) { func_rr(REG_B) }
u_int8_t rr_c(struct zb_bus * bus, struct zb_registers * regs) { func_rr(REG_C) }
u_int8_t rr_d(struct zb_bus * bus, struct zb_registers * regs) { func_rr(REG_D) }
u_int8_t rr_e(struct zb_bus * bus, struct zb_registers * regs) { func_rr(REG_E) }
u_int8_t rr_h(struct zb_bus * bus, struct zb_registers * regs) { func_rr(REG_H) }
u_int8_t rr_l(struct zb_bus * bus, struct zb_registers * regs) { func_rr(REG_L) }
u_int8_t rr_hl(struct zb_bus * bus, struct zb_registers * regs)
{
  zb_byte value = bus_read8(bus, REG_HL(regs));
  bool bit0 = value & 0x80;

  value = value >> 1 | (HAS_FLAG_C(regs) ? 0x80 : 0);
  bus_write8(bus, REG_HL(regs), value);
  REG_F(regs) = (bit0 ? ZB_FLAG_C : 0) | (value == 0 ? ZB_FLAG_Z : 0);
  return 16;
}


#define func_sla_reg(rname)                                             \
  bool bit7 = rname(regs) & 0x80;                                       \
                                                                        \
  rname(regs) = rname(regs) << 1;                                       \
  REG_F(regs) = 0 | (rname(regs) == 0 ? ZB_FLAG_Z : 0) | (bit7 ? ZB_FLAG_C : 0); \
  return 8;

u_int8_t sla_a(struct zb_bus * bus, struct zb_registers * regs) { func_sla_reg(REG_A) }
u_int8_t sla_b(struct zb_bus * bus, struct zb_registers * regs) { func_sla_reg(REG_B) }
u_int8_t sla_c(struct zb_bus * bus, struct zb_registers * regs) { func_sla_reg(REG_C) }
u_int8_t sla_d(struct zb_bus * bus, struct zb_registers * regs) { func_sla_reg(REG_D) }
u_int8_t sla_e(struct zb_bus * bus, struct zb_registers * regs) { func_sla_reg(REG_E) }
u_int8_t sla_h(struct zb_bus * bus, struct zb_registers * regs) { func_sla_reg(REG_H) }
u_int8_t sla_l(struct zb_bus * bus, struct zb_registers * regs) { func_sla_reg(REG_L) }
u_int8_t sla_hl(struct zb_bus * bus, struct zb_registers * regs)
{
  zb_byte value = bus_read8(bus, REG_HL(regs));
  bool bit7 = value & 0x80;

  bus_write8(bus, REG_HL(regs), value << 1);
  REG_F(regs) = 0 | (value == 0 ? ZB_FLAG_Z : 0) | (bit7 ? ZB_FLAG_C : 0);
  return 8;
}

#define func_sra_reg(rname)                                             \
  bool bit0 = rname(regs) & 0x1;                                        \
                                                                        \
  rname(regs) = (rname(regs) >> 1) | (rname(regs) & 0x80);              \
  REG_F(regs) = 0 | (rname(regs) == 0 ? ZB_FLAG_Z : 0) | (bit0 ? ZB_FLAG_C : 0); \
  return 8;

u_int8_t sra_a(struct zb_bus * bus, struct zb_registers * regs) { func_sra_reg(REG_A) }
u_int8_t sra_b(struct zb_bus * bus, struct zb_registers * regs) { func_sra_reg(REG_B) }
u_int8_t sra_c(struct zb_bus * bus, struct zb_registers * regs) { func_sra_reg(REG_C) }
u_int8_t sra_d(struct zb_bus * bus, struct zb_registers * regs) { func_sra_reg(REG_D) }
u_int8_t sra_e(struct zb_bus * bus, struct zb_registers * regs) { func_sra_reg(REG_E) }
u_int8_t sra_h(struct zb_bus * bus, struct zb_registers * regs) { func_sra_reg(REG_H) }
u_int8_t sra_l(struct zb_bus * bus, struct zb_registers * regs) { func_sra_reg(REG_L) }
u_int8_t sra_hl(struct zb_bus * bus, struct zb_registers * regs)
{
  zb_byte value = bus_read8(bus, REG_HL(regs));
  bool bit0 = value & 0x1;

  bus_write8(bus, REG_HL(regs), (value >> 1) | (value & 0x80));
  REG_F(regs) = 0 | (value == 0 ? ZB_FLAG_Z : 0) | (bit0 ? ZB_FLAG_C : 0);
  return 8;
}

#define func_srl_reg(rname)                                             \
  bool bit0 = rname(regs) & 0x1;                                        \
                                                                        \
  rname(regs) = rname(regs) >> 1;                                       \
  REG_F(regs) = 0 | (rname(regs) == 0 ? ZB_FLAG_Z : 0) | (bit0 ? ZB_FLAG_C : 0); \
  return 8;

u_int8_t srl_a(struct zb_bus * bus, struct zb_registers * regs) { func_srl_reg(REG_A) }
u_int8_t srl_b(struct zb_bus * bus, struct zb_registers * regs) { func_srl_reg(REG_B) }
u_int8_t srl_c(struct zb_bus * bus, struct zb_registers * regs) { func_srl_reg(REG_C) }
u_int8_t srl_d(struct zb_bus * bus, struct zb_registers * regs) { func_srl_reg(REG_D) }
u_int8_t srl_e(struct zb_bus * bus, struct zb_registers * regs) { func_srl_reg(REG_E) }
u_int8_t srl_h(struct zb_bus * bus, struct zb_registers * regs) { func_srl_reg(REG_H) }
u_int8_t srl_l(struct zb_bus * bus, struct zb_registers * regs) { func_srl_reg(REG_L) }
u_int8_t srl_hl(struct zb_bus * bus, struct zb_registers * regs)
{
  zb_byte value = bus_read8(bus, REG_HL(regs));
  bool bit0 = value & 0x1;

  bus_write8(bus, REG_HL(regs), value >> 1);
  REG_F(regs) = 0 | (value == 0 ? ZB_FLAG_Z : 0) | (bit0 ? ZB_FLAG_C : 0);
  return 16;
}


#define func_swap_reg(rname)                                \
  rname(regs) = (rname(regs) << 4) | (rname(regs) >> 4);    \
  REG_F(regs) = rname(regs) == 0 ? ZB_FLAG_Z : 0;           \
  return 8;

u_int8_t swap_a(struct zb_bus * bus, struct zb_registers * regs) { func_swap_reg(REG_A) }
u_int8_t swap_b(struct zb_bus * bus, struct zb_registers * regs) { func_swap_reg(REG_B) }
u_int8_t swap_c(struct zb_bus * bus, struct zb_registers * regs) { func_swap_reg(REG_C) }
u_int8_t swap_d(struct zb_bus * bus, struct zb_registers * regs) { func_swap_reg(REG_D) }
u_int8_t swap_e(struct zb_bus * bus, struct zb_registers * regs) { func_swap_reg(REG_E) }
u_int8_t swap_h(struct zb_bus * bus, struct zb_registers * regs) { func_swap_reg(REG_H) }
u_int8_t swap_l(struct zb_bus * bus, struct zb_registers * regs) { func_swap_reg(REG_L) }
u_int8_t swap_hl(struct zb_bus * bus, struct zb_registers * regs)
{
  zb_byte value = bus_read8(bus, REG_HL(regs));

  bus_write8(bus, REG_HL(regs), (value << 4) | (value >> 4));
  REG_F(regs) = value == 0 ? ZB_FLAG_Z : 0;
  return 16;
}

#define func_bit_reset_reg(rname, offset)       \
  rname(regs) &= ~(1 << offset);                \
  return 8;

#define func_bit_reset_hl(offset)                                       \
  bus_write8(bus, REG_HL(regs), bus_read8(bus, REG_HL(regs)) & ~(1 << offset)); \
  return 16;

u_int8_t reset_0_a(struct zb_bus * bus, struct zb_registers * regs) { func_bit_reset_reg(REG_A, 0) }
u_int8_t reset_0_b(struct zb_bus * bus, struct zb_registers * regs) { func_bit_reset_reg(REG_B, 0) }
u_int8_t reset_0_c(struct zb_bus * bus, struct zb_registers * regs) { func_bit_reset_reg(REG_C, 0) }
u_int8_t reset_0_d(struct zb_bus * bus, struct zb_registers * regs) { func_bit_reset_reg(REG_D, 0) }
u_int8_t reset_0_e(struct zb_bus * bus, struct zb_registers * regs) { func_bit_reset_reg(REG_E, 0) }
u_int8_t reset_0_h(struct zb_bus * bus, struct zb_registers * regs) { func_bit_reset_reg(REG_H, 0) }
u_int8_t reset_0_l(struct zb_bus * bus, struct zb_registers * regs) { func_bit_reset_reg(REG_L, 0) }

u_int8_t reset_1_a(struct zb_bus * bus, struct zb_registers * regs) { func_bit_reset_reg(REG_A, 1) }
u_int8_t reset_1_b(struct zb_bus * bus, struct zb_registers * regs) { func_bit_reset_reg(REG_B, 1) }
u_int8_t reset_1_c(struct zb_bus * bus, struct zb_registers * regs) { func_bit_reset_reg(REG_C, 1) }
u_int8_t reset_1_d(struct zb_bus * bus, struct zb_registers * regs) { func_bit_reset_reg(REG_D, 1) }
u_int8_t reset_1_e(struct zb_bus * bus, struct zb_registers * regs) { func_bit_reset_reg(REG_E, 1) }
u_int8_t reset_1_h(struct zb_bus * bus, struct zb_registers * regs) { func_bit_reset_reg(REG_H, 1) }
u_int8_t reset_1_l(struct zb_bus * bus, struct zb_registers * regs) { func_bit_reset_reg(REG_L, 1) }

u_int8_t reset_2_a(struct zb_bus * bus, struct zb_registers * regs) { func_bit_reset_reg(REG_A, 2) }
u_int8_t reset_2_b(struct zb_bus * bus, struct zb_registers * regs) { func_bit_reset_reg(REG_B, 2) }
u_int8_t reset_2_c(struct zb_bus * bus, struct zb_registers * regs) { func_bit_reset_reg(REG_C, 2) }
u_int8_t reset_2_d(struct zb_bus * bus, struct zb_registers * regs) { func_bit_reset_reg(REG_D, 2) }
u_int8_t reset_2_e(struct zb_bus * bus, struct zb_registers * regs) { func_bit_reset_reg(REG_E, 2) }
u_int8_t reset_2_h(struct zb_bus * bus, struct zb_registers * regs) { func_bit_reset_reg(REG_H, 2) }
u_int8_t reset_2_l(struct zb_bus * bus, struct zb_registers * regs) { func_bit_reset_reg(REG_L, 2) }

u_int8_t reset_3_a(struct zb_bus * bus, struct zb_registers * regs) { func_bit_reset_reg(REG_A, 3) }
u_int8_t reset_3_b(struct zb_bus * bus, struct zb_registers * regs) { func_bit_reset_reg(REG_B, 3) }
u_int8_t reset_3_c(struct zb_bus * bus, struct zb_registers * regs) { func_bit_reset_reg(REG_C, 3) }
u_int8_t reset_3_d(struct zb_bus * bus, struct zb_registers * regs) { func_bit_reset_reg(REG_D, 3) }
u_int8_t reset_3_e(struct zb_bus * bus, struct zb_registers * regs) { func_bit_reset_reg(REG_E, 3) }
u_int8_t reset_3_h(struct zb_bus * bus, struct zb_registers * regs) { func_bit_reset_reg(REG_H, 3) }
u_int8_t reset_3_l(struct zb_bus * bus, struct zb_registers * regs) { func_bit_reset_reg(REG_L, 3) }

u_int8_t reset_4_a(struct zb_bus * bus, struct zb_registers * regs) { func_bit_reset_reg(REG_A, 4) }
u_int8_t reset_4_b(struct zb_bus * bus, struct zb_registers * regs) { func_bit_reset_reg(REG_B, 4) }
u_int8_t reset_4_c(struct zb_bus * bus, struct zb_registers * regs) { func_bit_reset_reg(REG_C, 4) }
u_int8_t reset_4_d(struct zb_bus * bus, struct zb_registers * regs) { func_bit_reset_reg(REG_D, 4) }
u_int8_t reset_4_e(struct zb_bus * bus, struct zb_registers * regs) { func_bit_reset_reg(REG_E, 4) }
u_int8_t reset_4_h(struct zb_bus * bus, struct zb_registers * regs) { func_bit_reset_reg(REG_H, 4) }
u_int8_t reset_4_l(struct zb_bus * bus, struct zb_registers * regs) { func_bit_reset_reg(REG_L, 4) }

u_int8_t reset_5_a(struct zb_bus * bus, struct zb_registers * regs) { func_bit_reset_reg(REG_A, 5) }
u_int8_t reset_5_b(struct zb_bus * bus, struct zb_registers * regs) { func_bit_reset_reg(REG_B, 5) }
u_int8_t reset_5_c(struct zb_bus * bus, struct zb_registers * regs) { func_bit_reset_reg(REG_C, 5) }
u_int8_t reset_5_d(struct zb_bus * bus, struct zb_registers * regs) { func_bit_reset_reg(REG_D, 5) }
u_int8_t reset_5_e(struct zb_bus * bus, struct zb_registers * regs) { func_bit_reset_reg(REG_E, 5) }
u_int8_t reset_5_h(struct zb_bus * bus, struct zb_registers * regs) { func_bit_reset_reg(REG_H, 5) }
u_int8_t reset_5_l(struct zb_bus * bus, struct zb_registers * regs) { func_bit_reset_reg(REG_L, 5) }

u_int8_t reset_6_a(struct zb_bus * bus, struct zb_registers * regs) { func_bit_reset_reg(REG_A, 6) }
u_int8_t reset_6_b(struct zb_bus * bus, struct zb_registers * regs) { func_bit_reset_reg(REG_B, 6) }
u_int8_t reset_6_c(struct zb_bus * bus, struct zb_registers * regs) { func_bit_reset_reg(REG_C, 6) }
u_int8_t reset_6_d(struct zb_bus * bus, struct zb_registers * regs) { func_bit_reset_reg(REG_D, 6) }
u_int8_t reset_6_e(struct zb_bus * bus, struct zb_registers * regs) { func_bit_reset_reg(REG_E, 6) }
u_int8_t reset_6_h(struct zb_bus * bus, struct zb_registers * regs) { func_bit_reset_reg(REG_H, 6) }
u_int8_t reset_6_l(struct zb_bus * bus, struct zb_registers * regs) { func_bit_reset_reg(REG_L, 6) }

u_int8_t reset_7_a(struct zb_bus * bus, struct zb_registers * regs) { func_bit_reset_reg(REG_A, 7) }
u_int8_t reset_7_b(struct zb_bus * bus, struct zb_registers * regs) { func_bit_reset_reg(REG_B, 7) }
u_int8_t reset_7_c(struct zb_bus * bus, struct zb_registers * regs) { func_bit_reset_reg(REG_C, 7) }
u_int8_t reset_7_d(struct zb_bus * bus, struct zb_registers * regs) { func_bit_reset_reg(REG_D, 7) }
u_int8_t reset_7_e(struct zb_bus * bus, struct zb_registers * regs) { func_bit_reset_reg(REG_E, 7) }
u_int8_t reset_7_h(struct zb_bus * bus, struct zb_registers * regs) { func_bit_reset_reg(REG_H, 7) }
u_int8_t reset_7_l(struct zb_bus * bus, struct zb_registers * regs) { func_bit_reset_reg(REG_L, 7) }

u_int8_t reset_0_hl(struct zb_bus * bus, struct zb_registers * regs) { func_bit_reset_hl(0) }
u_int8_t reset_1_hl(struct zb_bus * bus, struct zb_registers * regs) { func_bit_reset_hl(1) }
u_int8_t reset_2_hl(struct zb_bus * bus, struct zb_registers * regs) { func_bit_reset_hl(2) }
u_int8_t reset_3_hl(struct zb_bus * bus, struct zb_registers * regs) { func_bit_reset_hl(3) }
u_int8_t reset_4_hl(struct zb_bus * bus, struct zb_registers * regs) { func_bit_reset_hl(4) }
u_int8_t reset_5_hl(struct zb_bus * bus, struct zb_registers * regs) { func_bit_reset_hl(5) }
u_int8_t reset_6_hl(struct zb_bus * bus, struct zb_registers * regs) { func_bit_reset_hl(6) }
u_int8_t reset_7_hl(struct zb_bus * bus, struct zb_registers * regs) { func_bit_reset_hl(7) }

#define func_bit_set_reg(rname, offset)      \
  rname(regs) |= 1 << offset;                \
  return 8;

#define func_bit_set_hl(offset)                                         \
  bus_write8(bus, REG_HL(regs), bus_read8(bus, REG_HL(regs)) | 1 << offset); \
  return 16;

u_int8_t set_0_a(struct zb_bus * bus, struct zb_registers * regs) { func_bit_set_reg(REG_A, 0) }
u_int8_t set_0_b(struct zb_bus * bus, struct zb_registers * regs) { func_bit_set_reg(REG_B, 0) }
u_int8_t set_0_c(struct zb_bus * bus, struct zb_registers * regs) { func_bit_set_reg(REG_C, 0) }
u_int8_t set_0_d(struct zb_bus * bus, struct zb_registers * regs) { func_bit_set_reg(REG_D, 0) }
u_int8_t set_0_e(struct zb_bus * bus, struct zb_registers * regs) { func_bit_set_reg(REG_E, 0) }
u_int8_t set_0_h(struct zb_bus * bus, struct zb_registers * regs) { func_bit_set_reg(REG_H, 0) }
u_int8_t set_0_l(struct zb_bus * bus, struct zb_registers * regs) { func_bit_set_reg(REG_L, 0) }

u_int8_t set_1_a(struct zb_bus * bus, struct zb_registers * regs) { func_bit_set_reg(REG_A, 1) }
u_int8_t set_1_b(struct zb_bus * bus, struct zb_registers * regs) { func_bit_set_reg(REG_B, 1) }
u_int8_t set_1_c(struct zb_bus * bus, struct zb_registers * regs) { func_bit_set_reg(REG_C, 1) }
u_int8_t set_1_d(struct zb_bus * bus, struct zb_registers * regs) { func_bit_set_reg(REG_D, 1) }
u_int8_t set_1_e(struct zb_bus * bus, struct zb_registers * regs) { func_bit_set_reg(REG_E, 1) }
u_int8_t set_1_h(struct zb_bus * bus, struct zb_registers * regs) { func_bit_set_reg(REG_H, 1) }
u_int8_t set_1_l(struct zb_bus * bus, struct zb_registers * regs) { func_bit_set_reg(REG_L, 1) }

u_int8_t set_2_a(struct zb_bus * bus, struct zb_registers * regs) { func_bit_set_reg(REG_A, 2) }
u_int8_t set_2_b(struct zb_bus * bus, struct zb_registers * regs) { func_bit_set_reg(REG_B, 2) }
u_int8_t set_2_c(struct zb_bus * bus, struct zb_registers * regs) { func_bit_set_reg(REG_C, 2) }
u_int8_t set_2_d(struct zb_bus * bus, struct zb_registers * regs) { func_bit_set_reg(REG_D, 2) }
u_int8_t set_2_e(struct zb_bus * bus, struct zb_registers * regs) { func_bit_set_reg(REG_E, 2) }
u_int8_t set_2_h(struct zb_bus * bus, struct zb_registers * regs) { func_bit_set_reg(REG_H, 2) }
u_int8_t set_2_l(struct zb_bus * bus, struct zb_registers * regs) { func_bit_set_reg(REG_L, 2) }

u_int8_t set_3_a(struct zb_bus * bus, struct zb_registers * regs) { func_bit_set_reg(REG_A, 3) }
u_int8_t set_3_b(struct zb_bus * bus, struct zb_registers * regs) { func_bit_set_reg(REG_B, 3) }
u_int8_t set_3_c(struct zb_bus * bus, struct zb_registers * regs) { func_bit_set_reg(REG_C, 3) }
u_int8_t set_3_d(struct zb_bus * bus, struct zb_registers * regs) { func_bit_set_reg(REG_D, 3) }
u_int8_t set_3_e(struct zb_bus * bus, struct zb_registers * regs) { func_bit_set_reg(REG_E, 3) }
u_int8_t set_3_h(struct zb_bus * bus, struct zb_registers * regs) { func_bit_set_reg(REG_H, 3) }
u_int8_t set_3_l(struct zb_bus * bus, struct zb_registers * regs) { func_bit_set_reg(REG_L, 3) }

u_int8_t set_4_a(struct zb_bus * bus, struct zb_registers * regs) { func_bit_set_reg(REG_A, 4) }
u_int8_t set_4_b(struct zb_bus * bus, struct zb_registers * regs) { func_bit_set_reg(REG_B, 4) }
u_int8_t set_4_c(struct zb_bus * bus, struct zb_registers * regs) { func_bit_set_reg(REG_C, 4) }
u_int8_t set_4_d(struct zb_bus * bus, struct zb_registers * regs) { func_bit_set_reg(REG_D, 4) }
u_int8_t set_4_e(struct zb_bus * bus, struct zb_registers * regs) { func_bit_set_reg(REG_E, 4) }
u_int8_t set_4_h(struct zb_bus * bus, struct zb_registers * regs) { func_bit_set_reg(REG_H, 4) }
u_int8_t set_4_l(struct zb_bus * bus, struct zb_registers * regs) { func_bit_set_reg(REG_L, 4) }

u_int8_t set_5_a(struct zb_bus * bus, struct zb_registers * regs) { func_bit_set_reg(REG_A, 5) }
u_int8_t set_5_b(struct zb_bus * bus, struct zb_registers * regs) { func_bit_set_reg(REG_B, 5) }
u_int8_t set_5_c(struct zb_bus * bus, struct zb_registers * regs) { func_bit_set_reg(REG_C, 5) }
u_int8_t set_5_d(struct zb_bus * bus, struct zb_registers * regs) { func_bit_set_reg(REG_D, 5) }
u_int8_t set_5_e(struct zb_bus * bus, struct zb_registers * regs) { func_bit_set_reg(REG_E, 5) }
u_int8_t set_5_h(struct zb_bus * bus, struct zb_registers * regs) { func_bit_set_reg(REG_H, 5) }
u_int8_t set_5_l(struct zb_bus * bus, struct zb_registers * regs) { func_bit_set_reg(REG_L, 5) }

u_int8_t set_6_a(struct zb_bus * bus, struct zb_registers * regs) { func_bit_set_reg(REG_A, 6) }
u_int8_t set_6_b(struct zb_bus * bus, struct zb_registers * regs) { func_bit_set_reg(REG_B, 6) }
u_int8_t set_6_c(struct zb_bus * bus, struct zb_registers * regs) { func_bit_set_reg(REG_C, 6) }
u_int8_t set_6_d(struct zb_bus * bus, struct zb_registers * regs) { func_bit_set_reg(REG_D, 6) }
u_int8_t set_6_e(struct zb_bus * bus, struct zb_registers * regs) { func_bit_set_reg(REG_E, 6) }
u_int8_t set_6_h(struct zb_bus * bus, struct zb_registers * regs) { func_bit_set_reg(REG_H, 6) }
u_int8_t set_6_l(struct zb_bus * bus, struct zb_registers * regs) { func_bit_set_reg(REG_L, 6) }

u_int8_t set_7_a(struct zb_bus * bus, struct zb_registers * regs) { func_bit_set_reg(REG_A, 7) }
u_int8_t set_7_b(struct zb_bus * bus, struct zb_registers * regs) { func_bit_set_reg(REG_B, 7) }
u_int8_t set_7_c(struct zb_bus * bus, struct zb_registers * regs) { func_bit_set_reg(REG_C, 7) }
u_int8_t set_7_d(struct zb_bus * bus, struct zb_registers * regs) { func_bit_set_reg(REG_D, 7) }
u_int8_t set_7_e(struct zb_bus * bus, struct zb_registers * regs) { func_bit_set_reg(REG_E, 7) }
u_int8_t set_7_h(struct zb_bus * bus, struct zb_registers * regs) { func_bit_set_reg(REG_H, 7) }
u_int8_t set_7_l(struct zb_bus * bus, struct zb_registers * regs) { func_bit_set_reg(REG_L, 7) }

u_int8_t set_0_hl(struct zb_bus * bus, struct zb_registers * regs) { func_bit_set_hl(0) }
u_int8_t set_1_hl(struct zb_bus * bus, struct zb_registers * regs) { func_bit_set_hl(1) }
u_int8_t set_2_hl(struct zb_bus * bus, struct zb_registers * regs) { func_bit_set_hl(2) }
u_int8_t set_3_hl(struct zb_bus * bus, struct zb_registers * regs) { func_bit_set_hl(3) }
u_int8_t set_4_hl(struct zb_bus * bus, struct zb_registers * regs) { func_bit_set_hl(4) }
u_int8_t set_5_hl(struct zb_bus * bus, struct zb_registers * regs) { func_bit_set_hl(5) }
u_int8_t set_6_hl(struct zb_bus * bus, struct zb_registers * regs) { func_bit_set_hl(6) }
u_int8_t set_7_hl(struct zb_bus * bus, struct zb_registers * regs) { func_bit_set_hl(7) }
