#include "bus.h"
#include "logging.h"
#include "zameboy.h"
#include <sys/types.h>
#include <stddef.h>

static zb_byte * get_buffer(struct zb_bus * bus, zb_addr addr)
{
  if (addr < 0x100 && !bus->boot_over)
    return bus->boot_rom + addr;
  else if (addr < 0x4000)
    return bus->rom_0 + addr;
  else if (addr < 0x8000)
    return bus->rom_1 + addr - 0x4000;
  else if (addr < 0xA000)
    return bus->vram + addr - 0x8000;
  else if (addr < 0xC000)
    return bus->ext_ram + addr - 0xA000;
  else if (addr < 0xD000)
    return bus->wram_0 + addr - 0xC000;
  else if (addr < 0xE000)
    return bus->wram_1 + addr - 0xD000;
  else if (addr < 0xFE00) {
    log_warning(LOG_BUS, "ECHO RAM is not supported");
    return NULL;
  }
  else if (addr < 0xFEA0)
    return bus->oam + addr - 0xFE00;
  else if (addr < 0xFF00) {
    log_warning(LOG_BUS, "Range 0xFEA0 - 0xFEFF is not usable");
    return NULL;
  }
  else if (addr < 0xFF80)
    return bus->io_regs + addr - 0xFF00;
  else if (addr < 0xFFFF)
    return bus->hram + addr - 0xFF80;
  else if (addr == 0xFFFF)
    return &bus->ie_reg;

  log_debug(LOG_BUS, "read out of reach: %#06x", addr);
  return NULL;
}

zb_byte bus_read8(const struct zb_bus * bus, zb_addr addr)
{
  const zb_byte * buf = get_buffer((struct zb_bus *)bus, addr);

  return buf ? *buf : 0;
}

zb_sbyte bus_read8_signed(const struct zb_bus * bus, zb_addr addr)
{
  const zb_byte * buf = get_buffer((struct zb_bus *)bus, addr);
  return buf ? *buf : 0;
}

u_int16_t bus_read16(const struct zb_bus * bus, zb_addr addr)
{
  const zb_byte * buf = get_buffer((struct zb_bus *)bus, addr);
  return buf ? *(u_int16_t *)buf : 0;
}

const zb_byte * bus_get_buffer(const struct zb_bus * bus, zb_addr addr)
{
  return get_buffer((struct zb_bus *)bus, addr);
}

void bus_write8(struct zb_bus * bus, zb_addr addr, zb_byte value)
{
  zb_byte * buf = get_buffer(bus, addr);

  if (buf == NULL) {
    log_warning(LOG_BUS, "Writting 8 bits at address %#06x is not supported (value %#06x).", addr, value);
    return ;
  }

  log_debug(LOG_BUS, "Writting 0x%02x at 0x%04x", value, addr);
  *buf = value;
}

void bus_write16(struct zb_bus * bus, zb_addr addr, zb_dbyte value)
{
  zb_byte * buf = get_buffer(bus, addr);

  if (buf == NULL) {
    log_warning(LOG_BUS, "Writting 16 bits at address %#06x is not supported (value %#06x).", addr, value);
    return ;
  }

  log_debug(LOG_BUS, "Writting 0x%04x at 0x%04x", value, addr);
  *(zb_dbyte *)buf = value;
}
