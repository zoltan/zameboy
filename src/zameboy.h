#ifndef ZAMEBOY_H
#define ZAMEBOY_H

#include <sys/types.h>
#include <stdbool.h>

typedef u_int16_t   zb_addr;

typedef u_int8_t    zb_byte;
typedef int8_t      zb_sbyte;
typedef u_int16_t   zb_dbyte;

typedef u_int8_t    zb_reg8;
typedef u_int16_t   zb_reg16;

typedef union {
  zb_reg8   r8[2];
  zb_reg16  r16;
} zb_split_reg;

enum zb_flags {
  ZB_FLAG_C = 1 << 4,
  ZB_FLAG_H = 1 << 5,
  ZB_FLAG_N = 1 << 6,
  ZB_FLAG_Z = 1 << 7,
};

struct zb_registers {
  zb_split_reg  af;
  zb_split_reg  bc;
  zb_split_reg  de;
  zb_split_reg  hl;

  zb_reg16      pc;
  zb_reg16      sp;
};

#define REG8_LOW(regs, name)    (regs->name.r8[0])
#define REG8_HIGH(regs, name)   (regs->name.r8[1])
#define REG16(regs, name)       (regs->name.r16)

#define REG_AF(regs) REG16(regs, af)
#define REG_A(regs)  REG8_HIGH(regs, af)
#define REG_F(regs)  REG8_LOW(regs, af)

#define REG_BC(regs) REG16(regs, bc)
#define REG_B(regs)  REG8_HIGH(regs, bc)
#define REG_C(regs)  REG8_LOW(regs, bc)

#define REG_DE(regs) REG16(regs, de)
#define REG_D(regs)  REG8_HIGH(regs, de)
#define REG_E(regs)  REG8_LOW(regs, de)

#define REG_HL(regs) REG16(regs, hl)
#define REG_H(regs)  REG8_HIGH(regs, hl)
#define REG_L(regs)  REG8_LOW(regs, hl)

#define REG_PC(regs) (regs->pc)
#define REG_SP(regs) (regs->sp)

#define HAS_FLAG_Z(regs) (REG_F(regs) & ZB_FLAG_Z)
#define HAS_FLAG_N(regs) (REG_F(regs) & ZB_FLAG_N)
#define HAS_FLAG_H(regs) (REG_F(regs) & ZB_FLAG_H)
#define HAS_FLAG_C(regs) (REG_F(regs) & ZB_FLAG_C)

struct zb_bus
{
  zb_byte   boot_rom[0xFF];
  zb_byte   rom_0[0x4000];
  zb_byte   rom_1[0x4000];
  zb_byte   vram[0x2000];
  zb_byte   ext_ram[0x2000];
  zb_byte   wram_0[0x1000];
  zb_byte   wram_1[0x1000];
  zb_byte   oam[0xA0];
  zb_byte   io_regs[0x80];
  zb_byte   hram[0x7E];
  zb_byte   ie_reg;

  bool  boot_over;
  bool  ime;        /* Interrupt Master Enable Flag */
  bool  halted;
  bool  stopped;

/* 0000 	3FFF 	16KB ROM bank 00 	From cartridge, usually a fixed bank */
/* 4000 	7FFF 	16KB ROM Bank 01~NN 	From cartridge, switchable bank via MB (if any) */
/* 8000 	9FFF 	8KB Video RAM (VRAM) 	Only bank 0 in Non-CGB mode Switchable bank 0/1 in CGB mode */

/* A000 	BFFF 	8KB External RAM 	In cartridge, switchable bank if any */
/* C000 	CFFF 	4KB Work RAM (WRAM) bank 0 	 */
/* D000 	DFFF 	4KB Work RAM (WRAM) bank 1~N 	Only bank 1 in Non-CGB mode Switchable bank 1~7 in CGB mode */
/* E000 	FDFF 	Mirror of C000~DDFF (ECHO RAM) 	Nintendo says use of this area is prohibited. */
/* FE00 	FE9F 	Sprite attribute table (OAM) 	 */
/* FEA0 	FEFF 	Not Usable 	Nintendo says use of this area is prohibited */
/* FF00 	FF7F 	I/O Registers 	 */
/* FF80 	FFFE 	High RAM (HRAM) 	 */
/* FFFF 	FFFF 	Interrupts Enable Register (IE) 	 */

};

struct zameboy
{
  struct zb_bus         bus;
  struct zb_registers   registers;
};

#endif /* ZAMEBOY_H */
