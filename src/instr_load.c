#include "bus.h"
#include "instruction.h"
#include "zameboy.h"
#include <sys/types.h>

u_int8_t load_bc_d16(struct zb_bus * bus, struct zb_registers * regs)
{
  zb_addr operand;

  operand = bus_read16(bus, regs->pc + 1);
  REG_BC(regs) = operand;
  return 12;
}

u_int8_t load_de_d16(struct zb_bus * bus, struct zb_registers * regs)
{
  zb_addr operand;

  operand = bus_read16(bus, regs->pc + 1);
  REG_DE(regs) = operand;
  return 12;
}

u_int8_t load_hl_d16(struct zb_bus * bus, struct zb_registers * regs)
{
  zb_addr operand;

  operand = bus_read16(bus, regs->pc + 1);
  REG_HL(regs) = operand;
  return 12;
}

u_int8_t load_sp_d16(struct zb_bus * bus, struct zb_registers * regs)
{
  zb_addr operand;

  operand = bus_read16(bus, regs->pc + 1);
  REG_SP(regs) = operand;
  return 12;
}

u_int8_t load_sp_hl(struct zb_bus * bus, struct zb_registers * regs)
{
  REG_SP(regs) = REG_HL(regs);
  return 8;
}

u_int8_t load_a16_sp(struct zb_bus * bus, struct zb_registers * regs)
{
  zb_addr operand;

  operand = bus_read16(bus, regs->pc + 1);

  bus_write16(bus, operand, REG_SP(regs));
  return 20;
}

u_int8_t load_hl_sp_r8(struct zb_bus * bus, struct zb_registers * regs)
{
  zb_sbyte operand;

  operand = bus_read8_signed(bus, REG_PC(regs) + 1);
  REG_HL(regs) = REG_SP(regs) + operand;

  REG_F(regs) = 0;
  if ((REG_SP(regs) & 0xF) + (operand & 0xF) > 0xF)
    REG_F(regs) |= ZB_FLAG_H;
  if ((REG_SP(regs) & 0xFF) + (operand & 0xFF) > 0xFF)
    REG_F(regs) |= ZB_FLAG_C;

  return 12;
}

u_int8_t push_bc(struct zb_bus * bus, struct zb_registers * regs)
{
  REG_SP(regs) -= 2;

  bus_write16(bus, REG_SP(regs), REG_BC(regs));
  return 16;
}

u_int8_t push_de(struct zb_bus * bus, struct zb_registers * regs)
{
  REG_SP(regs) -= 2;

  bus_write16(bus, REG_SP(regs), REG_DE(regs));
  return 16;
}

u_int8_t push_hl(struct zb_bus * bus, struct zb_registers * regs)
{
  REG_SP(regs) -= 2;

  bus_write16(bus, REG_SP(regs), REG_HL(regs));
  return 16;
}

u_int8_t push_af(struct zb_bus * bus, struct zb_registers * regs)
{
  REG_SP(regs) -= 2;

  bus_write16(bus, REG_SP(regs), REG_AF(regs));
  return 16;
}

u_int8_t pop_bc(struct zb_bus * bus, struct zb_registers * regs)
{
  REG_BC(regs) = bus_read16(bus, REG_SP(regs));
  REG_SP(regs) += 2;

  return 12;
}

u_int8_t pop_de(struct zb_bus * bus, struct zb_registers * regs)
{
  REG_DE(regs) = bus_read16(bus, REG_SP(regs));
  REG_SP(regs) += 2;

  return 12;
}

u_int8_t pop_hl(struct zb_bus * bus, struct zb_registers * regs)
{
  REG_HL(regs) = bus_read16(bus, REG_SP(regs));
  REG_SP(regs) += 2;

  return 12;
}

u_int8_t pop_af(struct zb_bus * bus, struct zb_registers * regs)
{
  REG_AF(regs) = bus_read16(bus, REG_SP(regs));
  REG_SP(regs) += 2;

  /* the lower 4 bits must be 0 as they are flags. */
  REG_F(regs) &= 0xF0;

  return 12;
}

u_int8_t load_bc_a(struct zb_bus * bus, struct zb_registers * regs)      { bus_write8(bus, REG_BC(regs), REG_A(regs)); return 8; }
u_int8_t load_de_a(struct zb_bus * bus, struct zb_registers * regs)      { bus_write8(bus, REG_DE(regs), REG_A(regs)); return 8; }
u_int8_t load_hlplus_a(struct zb_bus * bus, struct zb_registers * regs)  { bus_write8(bus, REG_HL(regs)++, REG_A(regs)); return 8; }
u_int8_t load_hlminus_a(struct zb_bus * bus, struct zb_registers * regs) { bus_write8(bus, REG_HL(regs)--, REG_A(regs)); return 8; }

u_int8_t load_a_bc(struct zb_bus * bus, struct zb_registers * regs)      { REG_A(regs) = bus_read8(bus, REG_BC(regs)); return 8; }
u_int8_t load_a_de(struct zb_bus * bus, struct zb_registers * regs)      { REG_A(regs) = bus_read8(bus, REG_DE(regs)); return 8; }
u_int8_t load_a_hlplus(struct zb_bus * bus, struct zb_registers * regs)  { REG_A(regs) = bus_read8(bus, REG_HL(regs)++); return 8; }
u_int8_t load_a_hlminus(struct zb_bus * bus, struct zb_registers * regs) { REG_A(regs) = bus_read8(bus, REG_HL(regs)--); return 8; }

u_int8_t load_a_high_c(struct zb_bus * bus, struct zb_registers * regs)
{
  REG_A(regs) = bus_read8(bus, 0xFF00 + REG_C(regs));
  return 8;
}

u_int8_t load_high_c_a(struct zb_bus * bus, struct zb_registers * regs)
{
  bus_write8(bus, 0xFF00 + REG_C(regs), REG_A(regs));
  return 8;
}

u_int8_t load_a_high_a8(struct zb_bus * bus, struct zb_registers * regs)
{
  zb_byte operand = bus_read8(bus, REG_PC(regs) + 1);

  REG_A(regs) = bus_read8(bus, 0xFF00 + operand);
  return 8;
}

u_int8_t load_high_a8_a(struct zb_bus * bus, struct zb_registers * regs)
{
  zb_byte operand = bus_read8(bus, REG_PC(regs) + 1);

  bus_write8(bus, 0xFF00 + operand, REG_A(regs));
  return 8;
}

#define func_load_reg_d8(rname) \
  REG_A(regs) = bus_read8(bus, REG_PC(regs) + 1); \
  return 8;

u_int8_t load_a_d8(struct zb_bus * bus, struct zb_registers * regs) { func_load_reg_d8(REG_A) }
u_int8_t load_b_d8(struct zb_bus * bus, struct zb_registers * regs) { func_load_reg_d8(REG_B) }
u_int8_t load_c_d8(struct zb_bus * bus, struct zb_registers * regs) { func_load_reg_d8(REG_C) }
u_int8_t load_d_d8(struct zb_bus * bus, struct zb_registers * regs) { func_load_reg_d8(REG_D) }
u_int8_t load_e_d8(struct zb_bus * bus, struct zb_registers * regs) { func_load_reg_d8(REG_E) }
u_int8_t load_h_d8(struct zb_bus * bus, struct zb_registers * regs) { func_load_reg_d8(REG_H) }
u_int8_t load_l_d8(struct zb_bus * bus, struct zb_registers * regs) { func_load_reg_d8(REG_L) }
u_int8_t load_hl_d8(struct zb_bus * bus, struct zb_registers * regs)
{
  zb_byte operand;

  operand = bus_read8(bus, REG_PC(regs) + 1);
  bus_write8(bus, REG_HL(regs), operand);
  return 12;
}

#define func_load_hl_r8(rname) \
  bus_write8(bus, REG_HL(regs), rname(regs)); \
  return 8;

u_int8_t load_hl_a(struct zb_bus * bus, struct zb_registers * regs) { func_load_hl_r8(REG_A) }
u_int8_t load_hl_b(struct zb_bus * bus, struct zb_registers * regs) { func_load_hl_r8(REG_B) }
u_int8_t load_hl_c(struct zb_bus * bus, struct zb_registers * regs) { func_load_hl_r8(REG_C) }
u_int8_t load_hl_d(struct zb_bus * bus, struct zb_registers * regs) { func_load_hl_r8(REG_D) }
u_int8_t load_hl_e(struct zb_bus * bus, struct zb_registers * regs) { func_load_hl_r8(REG_E) }
u_int8_t load_hl_h(struct zb_bus * bus, struct zb_registers * regs) { func_load_hl_r8(REG_H) }
u_int8_t load_hl_l(struct zb_bus * bus, struct zb_registers * regs) { func_load_hl_r8(REG_L) }


#define func_load_r8_r8(left, right) \
  left(regs) = right(regs); \
  return 4;

u_int8_t load_a_a(struct zb_bus * bus, struct zb_registers * regs) { func_load_r8_r8(REG_A, REG_A) }
u_int8_t load_a_b(struct zb_bus * bus, struct zb_registers * regs) { func_load_r8_r8(REG_A, REG_B) }
u_int8_t load_a_c(struct zb_bus * bus, struct zb_registers * regs) { func_load_r8_r8(REG_A, REG_C) }
u_int8_t load_a_d(struct zb_bus * bus, struct zb_registers * regs) { func_load_r8_r8(REG_A, REG_D) }
u_int8_t load_a_e(struct zb_bus * bus, struct zb_registers * regs) { func_load_r8_r8(REG_A, REG_E) }
u_int8_t load_a_h(struct zb_bus * bus, struct zb_registers * regs) { func_load_r8_r8(REG_A, REG_H) }
u_int8_t load_a_l(struct zb_bus * bus, struct zb_registers * regs) { func_load_r8_r8(REG_A, REG_L) }

u_int8_t load_b_a(struct zb_bus * bus, struct zb_registers * regs) { func_load_r8_r8(REG_B, REG_A) }
u_int8_t load_b_b(struct zb_bus * bus, struct zb_registers * regs) { func_load_r8_r8(REG_B, REG_B) }
u_int8_t load_b_c(struct zb_bus * bus, struct zb_registers * regs) { func_load_r8_r8(REG_B, REG_C) }
u_int8_t load_b_d(struct zb_bus * bus, struct zb_registers * regs) { func_load_r8_r8(REG_B, REG_D) }
u_int8_t load_b_e(struct zb_bus * bus, struct zb_registers * regs) { func_load_r8_r8(REG_B, REG_E) }
u_int8_t load_b_h(struct zb_bus * bus, struct zb_registers * regs) { func_load_r8_r8(REG_B, REG_H) }
u_int8_t load_b_l(struct zb_bus * bus, struct zb_registers * regs) { func_load_r8_r8(REG_B, REG_L) }

u_int8_t load_c_a(struct zb_bus * bus, struct zb_registers * regs) { func_load_r8_r8(REG_C, REG_A) }
u_int8_t load_c_b(struct zb_bus * bus, struct zb_registers * regs) { func_load_r8_r8(REG_C, REG_B) }
u_int8_t load_c_c(struct zb_bus * bus, struct zb_registers * regs) { func_load_r8_r8(REG_C, REG_C) }
u_int8_t load_c_d(struct zb_bus * bus, struct zb_registers * regs) { func_load_r8_r8(REG_C, REG_D) }
u_int8_t load_c_e(struct zb_bus * bus, struct zb_registers * regs) { func_load_r8_r8(REG_C, REG_E) }
u_int8_t load_c_h(struct zb_bus * bus, struct zb_registers * regs) { func_load_r8_r8(REG_C, REG_H) }
u_int8_t load_c_l(struct zb_bus * bus, struct zb_registers * regs) { func_load_r8_r8(REG_C, REG_L) }

u_int8_t load_d_a(struct zb_bus * bus, struct zb_registers * regs) { func_load_r8_r8(REG_D, REG_A) }
u_int8_t load_d_b(struct zb_bus * bus, struct zb_registers * regs) { func_load_r8_r8(REG_D, REG_B) }
u_int8_t load_d_c(struct zb_bus * bus, struct zb_registers * regs) { func_load_r8_r8(REG_D, REG_C) }
u_int8_t load_d_d(struct zb_bus * bus, struct zb_registers * regs) { func_load_r8_r8(REG_D, REG_D) }
u_int8_t load_d_e(struct zb_bus * bus, struct zb_registers * regs) { func_load_r8_r8(REG_D, REG_E) }
u_int8_t load_d_h(struct zb_bus * bus, struct zb_registers * regs) { func_load_r8_r8(REG_D, REG_H) }
u_int8_t load_d_l(struct zb_bus * bus, struct zb_registers * regs) { func_load_r8_r8(REG_D, REG_L) }

u_int8_t load_e_a(struct zb_bus * bus, struct zb_registers * regs) { func_load_r8_r8(REG_E, REG_A) }
u_int8_t load_e_b(struct zb_bus * bus, struct zb_registers * regs) { func_load_r8_r8(REG_E, REG_B) }
u_int8_t load_e_c(struct zb_bus * bus, struct zb_registers * regs) { func_load_r8_r8(REG_E, REG_C) }
u_int8_t load_e_d(struct zb_bus * bus, struct zb_registers * regs) { func_load_r8_r8(REG_E, REG_D) }
u_int8_t load_e_e(struct zb_bus * bus, struct zb_registers * regs) { func_load_r8_r8(REG_E, REG_E) }
u_int8_t load_e_h(struct zb_bus * bus, struct zb_registers * regs) { func_load_r8_r8(REG_E, REG_H) }
u_int8_t load_e_l(struct zb_bus * bus, struct zb_registers * regs) { func_load_r8_r8(REG_E, REG_L) }

u_int8_t load_h_a(struct zb_bus * bus, struct zb_registers * regs) { func_load_r8_r8(REG_H, REG_A) }
u_int8_t load_h_b(struct zb_bus * bus, struct zb_registers * regs) { func_load_r8_r8(REG_H, REG_B) }
u_int8_t load_h_c(struct zb_bus * bus, struct zb_registers * regs) { func_load_r8_r8(REG_H, REG_C) }
u_int8_t load_h_d(struct zb_bus * bus, struct zb_registers * regs) { func_load_r8_r8(REG_H, REG_D) }
u_int8_t load_h_e(struct zb_bus * bus, struct zb_registers * regs) { func_load_r8_r8(REG_H, REG_E) }
u_int8_t load_h_h(struct zb_bus * bus, struct zb_registers * regs) { func_load_r8_r8(REG_H, REG_H) }
u_int8_t load_h_l(struct zb_bus * bus, struct zb_registers * regs) { func_load_r8_r8(REG_H, REG_L) }

u_int8_t load_l_a(struct zb_bus * bus, struct zb_registers * regs) { func_load_r8_r8(REG_L, REG_A) }
u_int8_t load_l_b(struct zb_bus * bus, struct zb_registers * regs) { func_load_r8_r8(REG_L, REG_B) }
u_int8_t load_l_c(struct zb_bus * bus, struct zb_registers * regs) { func_load_r8_r8(REG_L, REG_C) }
u_int8_t load_l_d(struct zb_bus * bus, struct zb_registers * regs) { func_load_r8_r8(REG_L, REG_D) }
u_int8_t load_l_e(struct zb_bus * bus, struct zb_registers * regs) { func_load_r8_r8(REG_L, REG_E) }
u_int8_t load_l_h(struct zb_bus * bus, struct zb_registers * regs) { func_load_r8_r8(REG_L, REG_H) }
u_int8_t load_l_l(struct zb_bus * bus, struct zb_registers * regs) { func_load_r8_r8(REG_L, REG_L) }

#define func_load_r8_hl(rname) \
  rname(regs) = bus_read8(bus, REG_HL(regs));   \
  return 8;

u_int8_t load_a_hl(struct zb_bus * bus, struct zb_registers * regs) { func_load_r8_hl(REG_A) }
u_int8_t load_b_hl(struct zb_bus * bus, struct zb_registers * regs) { func_load_r8_hl(REG_B) }
u_int8_t load_c_hl(struct zb_bus * bus, struct zb_registers * regs) { func_load_r8_hl(REG_C) }
u_int8_t load_d_hl(struct zb_bus * bus, struct zb_registers * regs) { func_load_r8_hl(REG_D) }
u_int8_t load_e_hl(struct zb_bus * bus, struct zb_registers * regs) { func_load_r8_hl(REG_E) }
u_int8_t load_h_hl(struct zb_bus * bus, struct zb_registers * regs) { func_load_r8_hl(REG_H) }
u_int8_t load_l_hl(struct zb_bus * bus, struct zb_registers * regs) { func_load_r8_hl(REG_L) }

u_int8_t load_a16_a(struct zb_bus * bus, struct zb_registers * regs)
{
  bus_write16(bus,
              bus_read16(bus, REG_PC(regs) + 1),
              REG_A(regs));
  return 16;
}

u_int8_t load_a_a16(struct zb_bus * bus, struct zb_registers * regs)
{
  REG_A(regs) = bus_read8(bus, bus_read16(bus, REG_PC(regs) + 1));
  return 16;
}
