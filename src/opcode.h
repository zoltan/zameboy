#ifndef OPCODE_H
#define OPCODE_H

#include "zameboy.h"

struct instr_def
{
  u_int8_t length;
  u_int8_t max_cycles;
  u_int8_t (*implem)(struct zb_bus * bus, struct zb_registers * registers);
  const char * mnemonic;
};

const struct instr_def * fetch(struct zb_bus * bus, zb_reg16 pc);

#endif /* OPCODE_H */
