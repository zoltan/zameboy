#include <string.h>
#include <strings.h>
#include <sys/types.h>

#include "bus.h"
#include "cpu.h"
#include "instruction.h"
#include "logging.h"
#include "opcode.h"
#include "zameboy.h"


static const struct instr_def _8b_opcodes[] = {

  {1, 4, noop, "NOP"}, // 0x00
  {3, 12, load_bc_d16, "LD BC,d16"}, // 0x01
  {1, 8, load_bc_a, "LD (BC),A"}, // 0x02
  {1, 8, inc16_bc, "INC BC"}, // 0x03
  {1, 4, inc8_b, "INC B"}, // 0x04
  {1, 4, dec8_b, "DEC B"}, // 0x05
  {2, 8, load_b_d8, "LD B,d8"}, // 0x06
  {1, 4, rlca, "RLCA"}, // 0x07
  {3, 20, load_a16_sp, "LD (a16),SP"}, // 0x08
  {1, 8, add_hl_bc, "ADD HL,BC"}, // 0x09
  {1, 8, load_a_bc, "LD A,(BC)"}, // 0x0a
  {1, 8, dec16_bc, "DEC BC"}, // 0x0b
  {1, 4, inc8_c, "INC C"}, // 0x0c
  {1, 4, dec8_c, "DEC C"}, // 0x0d
  {2, 8, load_c_d8, "LD C,d8"}, // 0x0e
  {1, 4, rrca, "RRCA"}, // 0x0f

  {2, 4, stop, "STOP 0"}, // 0x10
  {3, 12, load_de_d16, "LD DE,d16"}, // 0x11
  {1, 8, load_de_a, "LD (DE),A"}, // 0x12
  {1, 8, inc16_de, "INC DE"}, // 0x13
  {1, 4, inc8_d, "INC D"}, // 0x14
  {1, 4, dec8_d, "DEC D"}, // 0x15
  {2, 8, load_d_d8, "LD D,d8"}, // 0x16
  {1, 4, rla, "RLA"}, // 0x17
  {2, 12, jr_r8, "JR r8"}, // 0x18
  {1, 8, add_hl_de, "ADD HL,DE"}, // 0x19
  {1, 8, load_a_de, "LD A,(DE)"}, // 0x1a
  {1, 8, dec16_de, "DEC DE"}, // 0x1b
  {1, 4, inc8_e, "INC E"}, // 0x1c
  {1, 4, dec8_e, "DEC E"}, // 0x1d
  {2, 8, load_e_d8, "LD E,d8"}, // 0x1e
  {1, 4, rra, "RRA"}, // 0x1f

  {2, 12, jr_nz_r8, "JR NZ,r8"}, // 0x20
  {3, 12, load_hl_d16, "LD HL,d16"}, // 0x21
  {1, 8, load_hlplus_a, "LD (HL+),A"}, // 0x22
  {1, 8, inc16_hl, "INC HL"}, // 0x23
  {1, 4, inc8_h, "INC H"}, // 0x24
  {1, 4, dec8_h, "DEC H"}, // 0x25
  {2, 8, load_h_d8, "LD H,d8"}, // 0x26
  {1, 4, daa, "DAA"}, // 0x27
  {2, 12, jr_z_r8, "JR Z,r8"}, // 0x28
  {1, 8, add_hl_hl, "ADD HL,HL"}, // 0x29
  {1, 8, load_a_hlplus, "LD A,(HL+)"}, // 0x2a
  {1, 8, dec16_hl, "DEC HL"}, // 0x2b
  {1, 4, inc8_l, "INC L"}, // 0x2c
  {1, 4, dec8_l, "DEC L"}, // 0x2d
  {2, 8, load_l_d8, "LD L,d8"}, // 0x2e
  {1, 4, cpl, "CPL"}, // 0x2f

  {2, 12, jr_nc_r8, "JR NC,r8"}, // 0x30
  {3, 12, load_sp_d16, "LD SP,d16"}, // 0x31
  {1, 8, load_hlminus_a, "LD (HL-),A"}, // 0x32
  {1, 8, dec16_sp, "INC SP"}, // 0x33
  {1, 12, inc8_hl, "INC (HL)"}, // 0x34
  {1, 12, dec8_hl, "DEC (HL)"}, // 0x35
  {2, 12, load_hl_d8, "LD (HL),d8"}, // 0x36
  {1, 4, scf, "SCF"}, // 0x37
  {2, 12, jr_c_r8, "JR C,r8"}, // 0x38
  {1, 8, add_hl_sp, "ADD HL,SP"}, // 0x39
  {1, 8, load_a_hlminus, "LD A,(HL-)"}, // 0x3a
  {1, 8, dec16_sp, "DEC SP"}, // 0x3b
  {1, 4, inc8_a, "INC A"}, // 0x3c
  {1, 4, dec8_a, "DEC A"}, // 0x3d
  {2, 8, load_a_d8, "LD A,d8"}, // 0x3e
  {1, 4, ccf, "CCF"}, // 0x3f

  {1, 4, load_b_b, "LD B,B"}, // 0x40
  {1, 4, load_b_c, "LD B,C"}, // 0x41
  {1, 4, load_b_d, "LD B,D"}, // 0x42
  {1, 4, load_b_e, "LD B,E"}, // 0x43
  {1, 4, load_b_h, "LD B,H"}, // 0x44
  {1, 4, load_b_l, "LD B,L"}, // 0x45
  {1, 8, load_b_hl, "LD B,(HL)"}, // 0x46
  {1, 4, load_b_a, "LD B,A"}, // 0x47
  {1, 4, load_c_b, "LD C,B"}, // 0x48
  {1, 4, load_c_c, "LD C,C"}, // 0x49
  {1, 4, load_c_d, "LD C,D"}, // 0x4a
  {1, 4, load_c_e, "LD C,E"}, // 0x4b
  {1, 4, load_c_h, "LD C,H"}, // 0x4c
  {1, 4, load_c_l, "LD C,L"}, // 0x4d
  {1, 8, load_c_hl, "LD C,(HL)"}, // 0x4e
  {1, 4, load_c_a, "LD C,A"}, // 0x4f

  {1, 4, load_d_b, "LD D,B"}, // 0x50
  {1, 4, load_d_c, "LD D,C"}, // 0x51
  {1, 4, load_d_d, "LD D,D"}, // 0x52
  {1, 4, load_d_e, "LD D,E"}, // 0x53
  {1, 4, load_d_h, "LD D,H"}, // 0x54
  {1, 4, load_d_l, "LD D,L"}, // 0x55
  {1, 8, load_d_hl, "LD D,(HL)"}, // 0x56
  {1, 4, load_d_a, "LD D,A"}, // 0x57
  {1, 4, load_e_b, "LD E,B"}, // 0x58
  {1, 4, load_e_c, "LD E,C"}, // 0x59
  {1, 4, load_e_d, "LD E,D"}, // 0x5a
  {1, 4, load_e_e, "LD E,E"}, // 0x5b
  {1, 4, load_e_h, "LD E,H"}, // 0x5c
  {1, 4, load_e_l, "LD E,L"}, // 0x5d
  {1, 8, load_e_hl, "LD E,(HL)"}, // 0x5e
  {1, 4, load_e_a, "LD E,A"}, // 0x5f

  {1, 4, load_h_b, "LD H,B"}, // 0x60
  {1, 4, load_h_c, "LD H,C"}, // 0x61
  {1, 4, load_h_d, "LD H,D"}, // 0x62
  {1, 4, load_h_e, "LD H,E"}, // 0x63
  {1, 4, load_h_h, "LD H,H"}, // 0x64
  {1, 4, load_h_l, "LD H,L"}, // 0x65
  {1, 8, load_h_hl, "LD H,(HL)"}, // 0x66
  {1, 4, load_h_a, "LD H,A"}, // 0x67
  {1, 4, load_l_b, "LD L,B"}, // 0x68
  {1, 4, load_l_c, "LD L,C"}, // 0x69
  {1, 4, load_l_d, "LD L,D"}, // 0x6a
  {1, 4, load_l_e, "LD L,E"}, // 0x6b
  {1, 4, load_l_h, "LD L,H"}, // 0x6c
  {1, 4, load_l_l, "LD L,L"}, // 0x6d
  {1, 8, load_l_hl, "LD L,(HL)"}, // 0x6e
  {1, 4, load_l_a, "LD L,A"}, // 0x6f

  {1, 8, load_hl_b, "LD (HL),B"}, // 0x70
  {1, 8, load_hl_c, "LD (HL),C"}, // 0x71
  {1, 8, load_hl_d, "LD (HL),D"}, // 0x72
  {1, 8, load_hl_e, "LD (HL),E"}, // 0x73
  {1, 8, load_hl_h, "LD (HL),H"}, // 0x74
  {1, 8, load_hl_l, "LD (HL),L"}, // 0x75
  {1, 4, halt, "HALT"}, // 0x76
  {1, 8, load_hl_a, "LD (HL),A"}, // 0x77
  {1, 4, load_a_b, "LD A,B"}, // 0x78
  {1, 4, load_a_c, "LD A,C"}, // 0x79
  {1, 4, load_a_d, "LD A,D"}, // 0x7a
  {1, 4, load_a_e, "LD A,E"}, // 0x7b
  {1, 4, load_a_h, "LD A,H"}, // 0x7c
  {1, 4, load_a_l, "LD A,L"}, // 0x7d
  {1, 8, load_a_hl, "LD A,(HL)"}, // 0x7e
  {1, 4, load_a_a, "LD A,A"}, // 0x7f

  {1, 4, add_a_b, "ADD A,B"}, // 0x80
  {1, 4, add_a_c, "ADD A,C"}, // 0x81
  {1, 4, add_a_d, "ADD A,D"}, // 0x82
  {1, 4, add_a_e, "ADD A,E"}, // 0x83
  {1, 4, add_a_h, "ADD A,H"}, // 0x84
  {1, 4, add_a_l, "ADD A,L"}, // 0x85
  {1, 8, add_a_hl, "ADD A,(HL)"}, // 0x86
  {1, 4, add_a_a, "ADD A,A"}, // 0x87
  {1, 4, adc_a_b, "ADC A,B"}, // 0x88
  {1, 4, adc_a_c, "ADC A,C"}, // 0x89
  {1, 4, adc_a_d, "ADC A,D"}, // 0x8a
  {1, 4, adc_a_e, "ADC A,E"}, // 0x8b
  {1, 4, adc_a_h, "ADC A,H"}, // 0x8c
  {1, 4, adc_a_l, "ADC A,L"}, // 0x8d
  {1, 8, adc_a_h, "ADC A,(HL)"}, // 0x8e
  {1, 4, adc_a_a, "ADC A,A"}, // 0x8f

  {1, 4, adc_a_b, "SUB B"}, // 0x90
  {1, 4, adc_a_c, "SUB C"}, // 0x91
  {1, 4, adc_a_d, "SUB D"}, // 0x92
  {1, 4, adc_a_e, "SUB E"}, // 0x93
  {1, 4, adc_a_h, "SUB H"}, // 0x94
  {1, 4, adc_a_l, "SUB L"}, // 0x95
  {1, 8, adc_a_hl, "SUB (HL)"}, // 0x96
  {1, 4, adc_a_a, "SUB A"}, // 0x97
  {1, 4, sbc_a_b, "SBC A,B"}, // 0x98
  {1, 4, sbc_a_c, "SBC A,C"}, // 0x99
  {1, 4, sbc_a_d, "SBC A,D"}, // 0x9a
  {1, 4, sbc_a_e, "SBC A,E"}, // 0x9b
  {1, 4, sbc_a_h, "SBC A,H"}, // 0x9c
  {1, 4, sbc_a_l, "SBC A,L"}, // 0x9d
  {1, 8, sbc_a_h, "SBC A,(HL)"}, // 0x9e
  {1, 4, sbc_a_a, "SBC A,A"}, // 0x9f

  {1, 4, and_b, "AND B"}, // 0xa0
  {1, 4, and_c, "AND C"}, // 0xa1
  {1, 4, and_d, "AND D"}, // 0xa2
  {1, 4, and_e, "AND E"}, // 0xa3
  {1, 4, and_h, "AND H"}, // 0xa4
  {1, 4, and_l, "AND L"}, // 0xa5
  {1, 8, and_hl, "AND (HL)"}, // 0xa6
  {1, 4, and_a, "AND A"}, // 0xa7
  {1, 4, xor_b, "XOR B"}, // 0xa8
  {1, 4, xor_c, "XOR C"}, // 0xa9
  {1, 4, xor_d, "XOR D"}, // 0xaa
  {1, 4, xor_e, "XOR E"}, // 0xab
  {1, 4, xor_h, "XOR H"}, // 0xac
  {1, 4, xor_l, "XOR L"}, // 0xad
  {1, 8, xor_hl, "XOR (HL)"}, // 0xae
  {1, 4, xor_a, "XOR A"}, // 0xaf

  {1, 4, or_b, "OR B"}, // 0xb0
  {1, 4, or_c, "OR C"}, // 0xb1
  {1, 4, or_d, "OR D"}, // 0xb2
  {1, 4, or_e, "OR E"}, // 0xb3
  {1, 4, or_h, "OR H"}, // 0xb4
  {1, 4, or_l, "OR L"}, // 0xb5
  {1, 8, or_hl, "OR (HL)"}, // 0xb6
  {1, 4, or_a, "OR A"}, // 0xb7
  {1, 4, cp_b, "CP B"}, // 0xb8
  {1, 4, cp_c, "CP C"}, // 0xb9
  {1, 4, cp_d, "CP D"}, // 0xba
  {1, 4, cp_e, "CP E"}, // 0xbb
  {1, 4, cp_h, "CP H"}, // 0xbc
  {1, 4, cp_l, "CP L"}, // 0xbd
  {1, 8, cp_hl, "CP (HL)"}, // 0xbe
  {1, 4, cp_a, "CP A"}, // 0xbf

  {1, 20, ret_nz, "RET NZ"}, // 0xc0
  {1, 12, pop_bc, "POP BC"}, // 0xc1
  {3, 16, jp_nz_a16, "JP NZ,a16"}, // 0xc2
  {3, 16, jp_a16, "JP a16"}, // 0xc3
  {3, 24, call_nz, "CALL NZ,a16"}, // 0xc4
  {1, 16, push_bc, "PUSH BC"}, // 0xc5
  {2, 8, add_a_d8, "ADD A,d8"}, // 0xc6
  {1, 16, rst_00h, "RST 00H"}, // 0xc7
  {1, 20, ret_z, "RET Z"}, // 0xc8
  {1, 16, ret, "RET"}, // 0xc9
  {3, 16, jp_z_a16, "JP Z,a16"}, // 0xca
  {1, 4, NULL, "PREFIX CB"}, // 0xcb
  {3, 24, call_z, "CALL Z,a16"}, // 0xcc
  {3, 24, call, "CALL a16"}, // 0xcd
  {2, 8, adc_a_d8, "ADC A,d8"}, // 0xce
  {1, 16, rst_08h, "RST 08H"}, // 0xcf

  {1, 20, ret_nc, "RET NC"}, // 0xd0
  {1, 12, pop_de, "POP DE"}, // 0xd1
  {3, 16, jp_nc_a16, "JP NC,a16"}, // 0xd2
  {1, 1, NULL, "XXX"}, // 0xd3
  {3, 24, call_nc, "CALL NC,a16"}, // 0xd4
  {1, 16, push_de, "PUSH DE"}, // 0xd5
  {2, 8, sub_a_d8, "SUB d8"}, // 0xd6
  {1, 16, rst_10h, "RST 10H"}, // 0xd7
  {1, 20, ret_c, "RET C"}, // 0xd8
  {1, 16, reti, "RETI"}, // 0xd9
  {3, 16, jp_c_a16, "JP C,a16"}, // 0xda
  {1, 1, NULL, "XXX"}, // 0xdb
  {3, 24, call_c, "CALL C,a16"}, // 0xdc
  {1, 1, NULL, "XXX"}, // 0xdd
  {2, 8, sbc_a_d8, "SBC A,d8"}, // 0xde
  {1, 16, rst_18h, "RST 18H"}, // 0xdf

  {2, 12, load_high_a8_a, "LDH (a8),A"}, // 0xe0
  {1, 12, pop_hl, "POP HL"}, // 0x1
  {1, 8, load_high_c_a, "LD (C),A"}, // 0xe2
  {1, 1, NULL, "XXX"}, // 0xe3
  {1, 1, NULL, "XXX"}, // 0xe4
  {1, 16, push_hl, "PUSH HL"}, // 0xe5
  {2, 8, and_d8, "AND d8"}, // 0xe6
  {1, 16, rst_20h, "RST 20H"}, // 0xe7
  {2, 16, add_sp_r8, "ADD SP,r8"}, // 0xe8
  {1, 4, jp_hl, "JP (HL)"}, // 0xe9
  {3, 16, load_a16_a, "LD (a16),A"}, // 0xea
  {1, 1, NULL, "XXX"}, // 0xeb
  {1, 1, NULL, "XXX"}, // 0xec
  {1, 1, NULL, "XXX"}, // 0xed
  {2, 8, xor_d8, "XOR d8"}, // 0xee
  {1, 16, rst_28h, "RST 28H"}, // 0xef

  {2, 12, load_a_high_a8, "LDH A,(a8)"}, // 0xf0
  {1, 12, pop_af, "POP AF"}, // 0xf1
  {1, 8, load_a_high_c, "LD A,(C)"}, // 0xf2
  {1, 4, di, "DI"}, // 0xf3
  {1, 1, NULL, "XXX"}, // 0xf4
  {1, 16, push_af, "PUSH AF"}, // 0xf5
  {2, 8, or_d8, "OR d8"}, // 0xf6
  {1, 16, rst_30h, "RST 30H"}, // 0xf7
  {2, 12, load_hl_sp_r8, "LD HL,SP+r8"}, // 0xf8
  {1, 8, load_sp_hl, "LD SP,HL"}, // 0xf9
  {3, 16, load_a_a16, "LD A,(a16)"}, // 0xfa
  {1, 4, ei, "EI"}, // 0xfb
  {1, 1, NULL, "XXX"}, // 0xfc
  {1, 1, NULL, "XXX"}, // 0xfd
  {2, 8, cp_d8, "CP d8"}, // 0xfe
  {1, 16, rst_38h, "RST 38H"}, // 0xff

};

static const struct instr_def _16b_opcodes[] = {

  {2,  8, rlc_b, "RLC B"},
  {2,  8, rlc_c, "RLC C"},
  {2,  8, rlc_d, "RLC D"},
  {2,  8, rlc_e, "RLC E"},
  {2,  8, rlc_h, "RLC H"},
  {2,  8, rlc_l, "RLC L"},
  {2,  16, rlc_hl, "RLC (HL)"},
  {2,  8, rlc_a, "RLC A"},
  {2,  8, rrc_b, "RRC B"},
  {2,  8, rrc_c, "RRC C"},
  {2,  8, rrc_d, "RRC D"},
  {2,  8, rrc_e, "RRC E"},
  {2,  8, rrc_h, "RRC H"},
  {2,  8, rrc_l, "RRC L"},
  {2,  16, rrc_hl, "RRC (HL)"},
  {2,  8, rrc_a, "RRC A"},

  {2,  8, rl_b, "RL B"},
  {2,  8, rl_c, "RL C"},
  {2,  8, rl_d, "RL D"},
  {2,  8, rl_e, "RL E"},
  {2,  8, rl_h, "RL H"},
  {2,  8, rl_l, "RL L"},
  {2,  16, rl_hl, "RL (HL)"},
  {2,  8, rl_a, "RL A"},
  {2,  8, rr_b, "RR B"},
  {2,  8, rr_c, "RR C"},
  {2,  8, rr_d, "RR D"},
  {2,  8, rr_e, "RR E"},
  {2,  8, rr_h, "RR H"},
  {2,  8, rr_l, "RR L"},
  {2,  16, rr_hl, "RR (HL)"},
  {2,  8, rr_a, "RR A"},

  {2,  8, sla_b, "SLA B"},
  {2,  8, sla_c, "SLA C"},
  {2,  8, sla_d, "SLA D"},
  {2,  8, sla_e, "SLA E"},
  {2,  8, sla_h, "SLA H"},
  {2,  8, sla_l, "SLA L"},
  {2,  16, sla_hl, "SLA (HL)"},
  {2,  8, sla_a, "SLA A"},
  {2,  8, sra_b, "SRA B"},
  {2,  8, sra_c, "SRA C"},
  {2,  8, sra_d, "SRA D"},
  {2,  8, sra_e, "SRA E"},
  {2,  8, sra_h, "SRA H"},
  {2,  8, sra_l, "SRA L"},
  {2,  16, sra_hl, "SRA (HL)"},
  {2,  8, sra_a, "SRA A"},

  {2,  8, swap_b, "SWAP B"},
  {2,  8, swap_c, "SWAP C"},
  {2,  8, swap_d, "SWAP D"},
  {2,  8, swap_e, "SWAP E"},
  {2,  8, swap_h, "SWAP H"},
  {2,  8, swap_l, "SWAP L"},
  {2,  16, swap_hl, "SWAP (HL)"},
  {2,  8, swap_a, "SWAP A"},
  {2,  8, srl_b, "SRL B"},
  {2,  8, srl_c, "SRL C"},
  {2,  8, srl_d, "SRL D"},
  {2,  8, srl_e, "SRL E"},
  {2,  8, srl_h, "SRL H"},
  {2,  8, srl_l, "SRL L"},
  {2,  16, srl_hl, "SRL (HL)"},
  {2,  8, srl_a, "SRL A"},

  {2,  8, bit0_b, "BIT 0,B"},
  {2,  8, bit0_c, "BIT 0,C"},
  {2,  8, bit0_d, "BIT 0,D"},
  {2,  8, bit0_e, "BIT 0,E"},
  {2,  8, bit0_h, "BIT 0,H"},
  {2,  8, bit0_l, "BIT 0,L"},
  {2,  16, bit0_hl, "BIT 0,(HL)"},
  {2,  8, bit0_a, "BIT 0,A"},
  {2,  8, bit1_b, "BIT 1,B"},
  {2,  8, bit1_c, "BIT 1,C"},
  {2,  8, bit1_d, "BIT 1,D"},
  {2,  8, bit1_e, "BIT 1,E"},
  {2,  8, bit1_h, "BIT 1,H"},
  {2,  8, bit1_l, "BIT 1,L"},
  {2,  16, bit1_hl, "BIT 1,(HL)"},
  {2,  8, bit1_a, "BIT 1,A"},

  {2,  8, bit2_b, "BIT 2,B"},
  {2,  8, bit2_c, "BIT 2,C"},
  {2,  8, bit2_d, "BIT 2,D"},
  {2,  8, bit2_e, "BIT 2,E"},
  {2,  8, bit2_h, "BIT 2,H"},
  {2,  8, bit2_l, "BIT 2,L"},
  {2,  16, bit2_hl, "BIT 2,(HL)"},
  {2,  8, bit2_a, "BIT 2,A"},
  {2,  8, bit3_b, "BIT 3,B"},
  {2,  8, bit3_c, "BIT 3,C"},
  {2,  8, bit3_d, "BIT 3,D"},
  {2,  8, bit3_e, "BIT 3,E"},
  {2,  8, bit3_h, "BIT 3,H"},
  {2,  8, bit3_l, "BIT 3,L"},
  {2,  16, bit3_hl, "BIT 3,(HL)"},
  {2,  8, bit3_a, "BIT 3,A"},

  {2,  8, bit4_b, "BIT 4,B"},
  {2,  8, bit4_c, "BIT 4,C"},
  {2,  8, bit4_d, "BIT 4,D"},
  {2,  8, bit4_e, "BIT 4,E"},
  {2,  8, bit4_h, "BIT 4,H"},
  {2,  8, bit4_l, "BIT 4,L"},
  {2,  16, bit4_hl, "BIT 4,(HL)"},
  {2,  8, bit4_a, "BIT 4,A"},
  {2,  8, bit5_b, "BIT 5,B"},
  {2,  8, bit5_c, "BIT 5,C"},
  {2,  8, bit5_d, "BIT 5,D"},
  {2,  8, bit5_e, "BIT 5,E"},
  {2,  8, bit5_h, "BIT 5,H"},
  {2,  8, bit5_l, "BIT 5,L"},
  {2,  16, bit5_hl, "BIT 5,(HL)"},
  {2,  8, bit5_a, "BIT 5,A"},

  {2,  8, bit6_b, "BIT 6,B"},
  {2,  8, bit6_c, "BIT 6,C"},
  {2,  8, bit6_d, "BIT 6,D"},
  {2,  8, bit6_e, "BIT 6,E"},
  {2,  8, bit6_h, "BIT 6,H"},
  {2,  8, bit6_l, "BIT 6,L"},
  {2,  16, bit6_hl, "BIT 6,(HL)"},
  {2,  8, bit6_a, "BIT 6,A"},
  {2,  8, bit7_b, "BIT 7,B"},
  {2,  8, bit7_c, "BIT 7,C"},
  {2,  8, bit7_d, "BIT 7,D"},
  {2,  8, bit7_e, "BIT 7,E"},
  {2,  8, bit7_h, "BIT 7,H"},
  {2,  8, bit7_l, "BIT 7,L"},
  {2,  16, bit7_hl, "BIT 7,(HL)"},
  {2,  8, bit7_a, "BIT 7,A"},

  {2,  8, reset_0_b, "RES 0,B"},
  {2,  8, reset_0_c, "RES 0,C"},
  {2,  8, reset_0_d, "RES 0,D"},
  {2,  8, reset_0_e, "RES 0,E"},
  {2,  8, reset_0_h, "RES 0,H"},
  {2,  8, reset_0_l, "RES 0,L"},
  {2,  16, reset_0_hl, "RES 0,(HL)"},
  {2,  8, reset_0_a, "RES 0,A"},
  {2,  8, reset_1_b, "RES 1,B"},
  {2,  8, reset_1_c, "RES 1,C"},
  {2,  8, reset_1_d, "RES 1,D"},
  {2,  8, reset_1_e, "RES 1,E"},
  {2,  8, reset_1_h, "RES 1,H"},
  {2,  8, reset_1_l, "RES 1,L"},
  {2,  16, reset_1_hl, "RES 1,(HL)"},
  {2,  8, reset_1_a, "RES 1,A"},

  {2,  8, reset_2_b, "RES 2,B"},
  {2,  8, reset_2_c, "RES 2,C"},
  {2,  8, reset_2_d, "RES 2,D"},
  {2,  8, reset_2_e, "RES 2,E"},
  {2,  8, reset_2_h, "RES 2,H"},
  {2,  8, reset_2_l, "RES 2,L"},
  {2,  16, reset_2_hl, "RES 2,(HL)"},
  {2,  8, reset_2_a, "RES 2,A"},
  {2,  8, reset_3_b, "RES 3,B"},
  {2,  8, reset_3_c, "RES 3,C"},
  {2,  8, reset_3_d, "RES 3,D"},
  {2,  8, reset_3_e, "RES 3,E"},
  {2,  8, reset_3_h, "RES 3,H"},
  {2,  8, reset_3_l, "RES 3,L"},
  {2,  16, reset_3_hl, "RES 3,(HL)"},
  {2,  8, reset_3_a, "RES 3,A"},

  {2,  8, reset_4_b, "RES 4,B"},
  {2,  8, reset_4_c, "RES 4,C"},
  {2,  8, reset_4_d, "RES 4,D"},
  {2,  8, reset_4_e, "RES 4,E"},
  {2,  8, reset_4_h, "RES 4,H"},
  {2,  8, reset_4_l, "RES 4,L"},
  {2,  16, reset_4_hl, "RES 4,(HL)"},
  {2,  8, reset_4_a, "RES 4,A"},
  {2,  8, reset_5_b, "RES 5,B"},
  {2,  8, reset_5_c, "RES 5,C"},
  {2,  8, reset_5_d, "RES 5,D"},
  {2,  8, reset_5_e, "RES 5,E"},
  {2,  8, reset_5_h, "RES 5,H"},
  {2,  8, reset_5_l, "RES 5,L"},
  {2,  16, reset_5_hl, "RES 5,(HL)"},
  {2,  8, reset_5_a, "RES 5,A"},

  {2,  8, reset_6_b, "RES 6,B"},
  {2,  8, reset_6_c, "RES 6,C"},
  {2,  8, reset_6_d, "RES 6,D"},
  {2,  8, reset_6_e, "RES 6,E"},
  {2,  8, reset_6_h, "RES 6,H"},
  {2,  8, reset_6_l, "RES 6,L"},
  {2,  16, reset_6_hl, "RES 6,(HL)"},
  {2,  8, reset_6_a, "RES 6,A"},
  {2,  8, reset_7_b, "RES 7,B"},
  {2,  8, reset_7_c, "RES 7,C"},
  {2,  8, reset_7_d, "RES 7,D"},
  {2,  8, reset_7_e, "RES 7,E"},
  {2,  8, reset_7_h, "RES 7,H"},
  {2,  8, reset_7_l, "RES 7,L"},
  {2,  16, reset_7_hl, "RES 7,(HL)"},
  {2,  8, reset_7_a, "RES 7,A"},

  {2,  8, set_0_b, "SET 0,B"},
  {2,  8, set_0_c, "SET 0,C"},
  {2,  8, set_0_d, "SET 0,D"},
  {2,  8, set_0_e, "SET 0,E"},
  {2,  8, set_0_h, "SET 0,H"},
  {2,  8, set_0_l, "SET 0,L"},
  {2,  16, set_0_hl, "SET 0,(HL)"},
  {2,  8, set_0_a, "SET 0,A"},
  {2,  8, set_1_b, "SET 1,B"},
  {2,  8, set_1_c, "SET 1,C"},
  {2,  8, set_1_d, "SET 1,D"},
  {2,  8, set_1_e, "SET 1,E"},
  {2,  8, set_1_h, "SET 1,H"},
  {2,  8, set_1_l, "SET 1,L"},
  {2,  16, set_1_hl, "SET 1,(HL)"},
  {2,  8, set_1_a, "SET 1,A"},

  {2,  8, set_2_b, "SET 2,B"},
  {2,  8, set_2_c, "SET 2,C"},
  {2,  8, set_2_d, "SET 2,D"},
  {2,  8, set_2_e, "SET 2,E"},
  {2,  8, set_2_h, "SET 2,H"},
  {2,  8, set_2_l, "SET 2,L"},
  {2,  16, set_2_hl, "SET 2,(HL)"},
  {2,  8, set_2_a, "SET 2,A"},
  {2,  8, set_3_b, "SET 3,B"},
  {2,  8, set_3_c, "SET 3,C"},
  {2,  8, set_3_d, "SET 3,D"},
  {2,  8, set_3_e, "SET 3,E"},
  {2,  8, set_3_h, "SET 3,H"},
  {2,  8, set_3_l, "SET 3,L"},
  {2,  16, set_3_hl, "SET 3,(HL)"},
  {2,  8, set_3_a, "SET 3,A"},

  {2,  8, set_4_b, "SET 4,B"},
  {2,  8, set_4_c, "SET 4,C"},
  {2,  8, set_4_d, "SET 4,D"},
  {2,  8, set_4_e, "SET 4,E"},
  {2,  8, set_4_h, "SET 4,H"},
  {2,  8, set_4_l, "SET 4,L"},
  {2,  16, set_4_hl, "SET 4,(HL)"},
  {2,  8, set_4_a, "SET 4,A"},
  {2,  8, set_5_b, "SET 5,B"},
  {2,  8, set_5_c, "SET 5,C"},
  {2,  8, set_5_d, "SET 5,D"},
  {2,  8, set_5_e, "SET 5,E"},
  {2,  8, set_5_h, "SET 5,H"},
  {2,  8, set_5_l, "SET 5,L"},
  {2,  16, set_5_hl, "SET 5,(HL)"},
  {2,  8, set_5_a, "SET 5,A"},

  {2,  8, set_6_b, "SET 6,B"},
  {2,  8, set_6_c, "SET 6,C"},
  {2,  8, set_6_d, "SET 6,D"},
  {2,  8, set_6_e, "SET 6,E"},
  {2,  8, set_6_h, "SET 6,H"},
  {2,  8, set_6_l, "SET 6,L"},
  {2,  16, set_6_hl, "SET 6,(HL)"},
  {2,  8, set_6_a, "SET 6,A"},
  {2,  8, set_7_b, "SET 7,B"},
  {2,  8, set_7_c, "SET 7,C"},
  {2,  8, set_7_d, "SET 7,D"},
  {2,  8, set_7_e, "SET 7,E"},
  {2,  8, set_7_h, "SET 7,H"},
  {2,  8, set_7_l, "SET 7,L"},
  {2,  16, set_7_hl, "SET 7,(HL)"},
  {2,  8, set_7_a, "SET 7,A"},

};

const struct instr_def * fetch(struct zb_bus * bus, zb_reg16 pc)
{
  zb_byte opcode;

  opcode = bus_read8(bus, pc);
  if (opcode == 0xCB) {
    opcode = bus_read8(bus, pc + 1);
    return &_16b_opcodes[opcode];
  }
  else
   return &_8b_opcodes[opcode];
}
