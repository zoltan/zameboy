#ifndef LOGGING_H
#define LOGGING_H

enum LogLevel {
  LOG_ERROR = 0,
  LOG_WARNING,
  LOG_INFO,
  LOG_DEBUG,
};

enum LogDomain {
  LOG_CORE = 0,
  LOG_CPU,
  LOG_BUS,
};

void logging_set_level(enum LogLevel);

__attribute__((__format__ (__printf__, 2, 3)))
void log_error(enum LogDomain domain, const char *fmt, ...);

__attribute__((__format__ (__printf__, 2, 3)))
void log_warning(enum LogDomain domain, const char *fmt, ...);

__attribute__((__format__ (__printf__, 2, 3)))
void log_info(enum LogDomain domain, const char *fmt, ...);

__attribute__((__format__ (__printf__, 2, 3)))
void log_debug(enum LogDomain domain, const char *fmt, ...);

#endif /* LOGGING_H */
