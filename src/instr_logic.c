#include "bus.h"
#include "instruction.h"
#include "zameboy.h"

#define func_and_reg(rname)                                         \
  REG_A(regs) &= rname(regs);                                       \
  REG_F(regs) = 0 | ZB_FLAG_H | (REG_A(regs) == 0 ? ZB_FLAG_Z : 0); \
  return 4;

u_int8_t and_a(struct zb_bus * bus, struct zb_registers * regs) { func_and_reg(REG_A) }
u_int8_t and_b(struct zb_bus * bus, struct zb_registers * regs) { func_and_reg(REG_B) }
u_int8_t and_c(struct zb_bus * bus, struct zb_registers * regs) { func_and_reg(REG_C) }
u_int8_t and_d(struct zb_bus * bus, struct zb_registers * regs) { func_and_reg(REG_D) }
u_int8_t and_e(struct zb_bus * bus, struct zb_registers * regs) { func_and_reg(REG_E) }
u_int8_t and_h(struct zb_bus * bus, struct zb_registers * regs) { func_and_reg(REG_H) }
u_int8_t and_l(struct zb_bus * bus, struct zb_registers * regs) { func_and_reg(REG_L) }
u_int8_t and_hl(struct zb_bus * bus, struct zb_registers * regs)
{
  zb_byte value;

  value = bus_read8(bus, REG_HL(regs));
  REG_A(regs) &= value;
  REG_F(regs) = 0 | ZB_FLAG_H | (REG_A(regs) == 0 ? ZB_FLAG_Z : 0);
  return 8;
}
u_int8_t and_d8(struct zb_bus * bus, struct zb_registers * regs)
{
  zb_byte value;

  value = bus_read8(bus, REG_PC(regs) + 1);
  REG_A(regs) &= value;
  REG_F(regs) = 0 | ZB_FLAG_H | (REG_A(regs) == 0 ? ZB_FLAG_Z : 0);
  return 8;
}

u_int8_t xor_a(struct zb_bus * bus, struct zb_registers * regs)
{
  REG_A(regs) = 0;
  REG_F(regs) = 0;
  REG_F(regs) |= ZB_FLAG_Z;
  return 4;
}

#define xor_func(rname) \
  REG_A(regs) ^= rname(regs); \
  REG_F(regs) = 0; \
  REG_F(regs) |= REG_A(regs) == 0 ? ZB_FLAG_Z : 0; \
  return 4;


u_int8_t xor_b(struct zb_bus * bus, struct zb_registers * regs) { xor_func(REG_B) }
u_int8_t xor_c(struct zb_bus * bus, struct zb_registers * regs) { xor_func(REG_C) }
u_int8_t xor_d(struct zb_bus * bus, struct zb_registers * regs) { xor_func(REG_D) }
u_int8_t xor_e(struct zb_bus * bus, struct zb_registers * regs) { xor_func(REG_E) }
u_int8_t xor_h(struct zb_bus * bus, struct zb_registers * regs) { xor_func(REG_H) }
u_int8_t xor_l(struct zb_bus * bus, struct zb_registers * regs) { xor_func(REG_L) }

u_int8_t xor_hl(struct zb_bus * bus, struct zb_registers * regs)
{
  REG_A(regs) ^= bus_read8(bus, REG_HL(regs));
  REG_F(regs) = 0;
  REG_F(regs) |= REG_A(regs) == 0 ? ZB_FLAG_Z : 0;
  return 8;
}

u_int8_t xor_d8(struct zb_bus * bus, struct zb_registers * regs)
{
  REG_A(regs) ^= bus_read8(bus, REG_PC(regs) + 1);
  REG_F(regs) = 0;
  REG_F(regs) |= REG_A(regs) == 0 ? ZB_FLAG_Z : 0;
  return 8;
}

#define func_or_reg(rname)                                          \
  REG_A(regs) |= rname(regs);                                       \
  REG_F(regs) = 0 | (REG_A(regs) == 0 ? ZB_FLAG_Z : 0);             \
  return 4;

u_int8_t or_a(struct zb_bus * bus, struct zb_registers * regs) { func_or_reg(REG_A) }
u_int8_t or_b(struct zb_bus * bus, struct zb_registers * regs) { func_or_reg(REG_B) }
u_int8_t or_c(struct zb_bus * bus, struct zb_registers * regs) { func_or_reg(REG_C) }
u_int8_t or_d(struct zb_bus * bus, struct zb_registers * regs) { func_or_reg(REG_D) }
u_int8_t or_e(struct zb_bus * bus, struct zb_registers * regs) { func_or_reg(REG_E) }
u_int8_t or_h(struct zb_bus * bus, struct zb_registers * regs) { func_or_reg(REG_H) }
u_int8_t or_l(struct zb_bus * bus, struct zb_registers * regs) { func_or_reg(REG_L) }
u_int8_t or_hl(struct zb_bus * bus, struct zb_registers * regs)
{
  zb_byte value;

  value = bus_read8(bus, REG_HL(regs));
  REG_A(regs) |= value;
  REG_F(regs) = 0 | (REG_A(regs) == 0 ? ZB_FLAG_Z : 0);
  return 8;
}
u_int8_t or_d8(struct zb_bus * bus, struct zb_registers * regs)
{
  zb_byte value;

  value = bus_read8(bus, REG_PC(regs) + 1);
  REG_A(regs) |= value;
  REG_F(regs) = 0 | (REG_A(regs) == 0 ? ZB_FLAG_Z : 0);
  return 8;
}

#define func_cp_reg(rname) \
  REG_F(regs) = 0 | ZB_FLAG_N;                                          \
  REG_F(regs) |= (REG_A(regs) == rname(regs)) ? ZB_FLAG_Z : 0;          \
  REG_F(regs) |= ((REG_A(regs) & (1 << 3)) < (rname(regs) & (1 << 3))) ? ZB_FLAG_H : 0; \
  REG_F(regs) |= (REG_A(regs) < rname(regs)) ? ZB_FLAG_C : 0;           \
  return 4;

u_int8_t cp_a(struct zb_bus * bus, struct zb_registers * regs) { func_cp_reg(REG_A) }
u_int8_t cp_b(struct zb_bus * bus, struct zb_registers * regs) { func_cp_reg(REG_B) }
u_int8_t cp_c(struct zb_bus * bus, struct zb_registers * regs) { func_cp_reg(REG_C) }
u_int8_t cp_d(struct zb_bus * bus, struct zb_registers * regs) { func_cp_reg(REG_D) }
u_int8_t cp_e(struct zb_bus * bus, struct zb_registers * regs) { func_cp_reg(REG_E) }
u_int8_t cp_h(struct zb_bus * bus, struct zb_registers * regs) { func_cp_reg(REG_H) }
u_int8_t cp_l(struct zb_bus * bus, struct zb_registers * regs) { func_cp_reg(REG_L) }
u_int8_t cp_hl(struct zb_bus * bus, struct zb_registers * regs)
{
  zb_byte value;

  value = bus_read8(bus, REG_HL(regs));
  REG_F(regs) = 0 | ZB_FLAG_N;
  REG_F(regs) |= (REG_A(regs) == value) ? ZB_FLAG_Z : 0;
  REG_F(regs) |= ((REG_A(regs) & (1 << 3)) < (value & (1 << 3))) ? ZB_FLAG_H : 0;
  REG_F(regs) |= (REG_A(regs) < value) ? ZB_FLAG_C : 0;
  return 8;
}
u_int8_t cp_d8(struct zb_bus * bus, struct zb_registers * regs)
{
  zb_byte value;

  value = bus_read8(bus, REG_HL(regs));
  REG_F(regs) = 0 | ZB_FLAG_N;
  REG_F(regs) |= (REG_A(regs) == value) ? ZB_FLAG_Z : 0;
  REG_F(regs) |= ((REG_A(regs) & (1 << 3)) < (value & (1 << 3))) ? ZB_FLAG_H : 0;
  REG_F(regs) |= (REG_A(regs) < value) ? ZB_FLAG_C : 0;
  return 8;
}


#define func_inc_reg(rname) \
  ++rname(regs); \
  REG_F(regs) &= 0x10; \
  REG_F(regs) |= (rname(regs) == 0 ? ZB_FLAG_Z : 0) | (((rname(regs) & 0x10) == 0x10) ? ZB_FLAG_H : 0); \
  return 4;

u_int8_t inc8_a(struct zb_bus * bus, struct zb_registers * regs) { func_inc_reg(REG_A) }
u_int8_t inc8_b(struct zb_bus * bus, struct zb_registers * regs) { func_inc_reg(REG_B) }
u_int8_t inc8_c(struct zb_bus * bus, struct zb_registers * regs) { func_inc_reg(REG_C) }
u_int8_t inc8_d(struct zb_bus * bus, struct zb_registers * regs) { func_inc_reg(REG_D) }
u_int8_t inc8_e(struct zb_bus * bus, struct zb_registers * regs) { func_inc_reg(REG_E) }
u_int8_t inc8_h(struct zb_bus * bus, struct zb_registers * regs) { func_inc_reg(REG_H) }
u_int8_t inc8_l(struct zb_bus * bus, struct zb_registers * regs) { func_inc_reg(REG_L) }
u_int8_t inc8_hl(struct zb_bus * bus, struct zb_registers * regs)
{
  zb_byte value;

  value = bus_read8(bus, REG_HL(regs)) + 1;
  bus_write8(bus, REG_HL(regs), value);

  REG_F(regs) &= 0x10;
  REG_F(regs) |= (value == 0 ? ZB_FLAG_Z : 0) | (((value & 0x10) == 0x10) ? ZB_FLAG_H : 0);

  return 12;
}

#define func_dec_reg(rname) \
  --rname(regs); \
  REG_F(regs) &= 0x10; \
  REG_F(regs) |= ZB_FLAG_N | (rname(regs) == 0 ? ZB_FLAG_Z : 0) | (((rname(regs) & 0x10) == 0x10) ? ZB_FLAG_H : 0); \
  return 4;

u_int8_t dec8_a(struct zb_bus * bus, struct zb_registers * regs) { func_dec_reg(REG_A) }
u_int8_t dec8_b(struct zb_bus * bus, struct zb_registers * regs) { func_dec_reg(REG_B) }
u_int8_t dec8_c(struct zb_bus * bus, struct zb_registers * regs) { func_dec_reg(REG_C) }
u_int8_t dec8_d(struct zb_bus * bus, struct zb_registers * regs) { func_dec_reg(REG_D) }
u_int8_t dec8_e(struct zb_bus * bus, struct zb_registers * regs) { func_dec_reg(REG_E) }
u_int8_t dec8_h(struct zb_bus * bus, struct zb_registers * regs) { func_dec_reg(REG_H) }
u_int8_t dec8_l(struct zb_bus * bus, struct zb_registers * regs) { func_dec_reg(REG_L) }
u_int8_t dec8_hl(struct zb_bus * bus, struct zb_registers * regs)
{
  zb_byte value;

  value = bus_read8(bus, REG_HL(regs)) - 1;
  bus_write8(bus, REG_HL(regs), value);

  REG_F(regs) &= 0x10;
  REG_F(regs) |= ZB_FLAG_N | (value == 0 ? ZB_FLAG_Z : 0) | (((value & 0x10) == 0x10) ? ZB_FLAG_H : 0);

  return 12;
}

u_int8_t inc16_bc(struct zb_bus * bus, struct zb_registers * regs) { ++REG_BC(regs); return 8; }
u_int8_t inc16_de(struct zb_bus * bus, struct zb_registers * regs) { ++REG_DE(regs); return 8; }
u_int8_t inc16_hl(struct zb_bus * bus, struct zb_registers * regs) { ++REG_HL(regs); return 8; }
u_int8_t inc16_sp(struct zb_bus * bus, struct zb_registers * regs) { ++REG_SP(regs); return 8; }

u_int8_t dec16_bc(struct zb_bus * bus, struct zb_registers * regs) { --REG_BC(regs); return 8; }
u_int8_t dec16_de(struct zb_bus * bus, struct zb_registers * regs) { --REG_DE(regs); return 8; }
u_int8_t dec16_hl(struct zb_bus * bus, struct zb_registers * regs) { --REG_HL(regs); return 8; }
u_int8_t dec16_sp(struct zb_bus * bus, struct zb_registers * regs) { --REG_SP(regs); return 8; }


#define func_add_hl_r16(rname) \
  zb_reg16 hl = REG_HL(regs); \
  zb_reg16 r16 = rname(regs); \
\
  REG_F(regs) = 0x0 | ZB_FLAG_Z; \
  REG_F(regs) |= (((hl & 0xFFF) + (r16 & 0xFFF)) & 0x1000) ? ZB_FLAG_H : 0; \
  REG_F(regs) |= ((unsigned int)hl + (unsigned)r16 > 0x10000) ? ZB_FLAG_C : 0; \
  REG_HL(regs) += r16; \
  return 8;

u_int8_t add_hl_bc(struct zb_bus * bus, struct zb_registers * regs) { func_add_hl_r16(REG_BC) }
u_int8_t add_hl_de(struct zb_bus * bus, struct zb_registers * regs) { func_add_hl_r16(REG_DE) }
u_int8_t add_hl_hl(struct zb_bus * bus, struct zb_registers * regs) { func_add_hl_r16(REG_HL) }
u_int8_t add_hl_sp(struct zb_bus * bus, struct zb_registers * regs) { func_add_hl_r16(REG_SP) }


#define func_add_a_r8(rname) \
  REG_F(regs) = 0; \
  REG_F(regs) |= ((REG_A(regs) & (1 << 3)) & (rname(regs) & (1 << 3))) ? ZB_FLAG_H : 0; \
  REG_F(regs) |= ((REG_A(regs) & (1 << 7)) & (rname(regs) & (1 << 7))) ? ZB_FLAG_C : 0; \
 \
  REG_A(regs) += rname(regs); \
  REG_F(regs) |= (REG_A(regs) == 0 ? ZB_FLAG_Z : 0); \
  return 4;

u_int8_t add_a_a(struct zb_bus * bus, struct zb_registers * regs) { func_add_a_r8(REG_A) }
u_int8_t add_a_b(struct zb_bus * bus, struct zb_registers * regs) { func_add_a_r8(REG_A) }
u_int8_t add_a_c(struct zb_bus * bus, struct zb_registers * regs) { func_add_a_r8(REG_A) }
u_int8_t add_a_d(struct zb_bus * bus, struct zb_registers * regs) { func_add_a_r8(REG_A) }
u_int8_t add_a_e(struct zb_bus * bus, struct zb_registers * regs) { func_add_a_r8(REG_A) }
u_int8_t add_a_h(struct zb_bus * bus, struct zb_registers * regs) { func_add_a_r8(REG_A) }
u_int8_t add_a_l(struct zb_bus * bus, struct zb_registers * regs) { func_add_a_r8(REG_A) }

u_int8_t add_a_hl(struct zb_bus * bus, struct zb_registers * regs)
{
  zb_byte value;

  value = bus_read8(bus, REG_HL(regs));

  REG_F(regs) = 0;
  REG_F(regs) |= ((REG_A(regs) & (1 << 3)) & (value & (1 << 3))) ? ZB_FLAG_H : 0;
  REG_F(regs) |= ((REG_A(regs) & (1 << 7)) & (value & (1 << 7))) ? ZB_FLAG_C : 0;

  REG_A(regs) += value;
  REG_F(regs) |= (REG_A(regs) == 0 ? ZB_FLAG_Z : 0);
  return 8;
}

u_int8_t add_a_d8(struct zb_bus * bus, struct zb_registers * regs)
{
  zb_byte value;

  value = bus_read8(bus, REG_PC(regs) + 1);

  REG_F(regs) = 0;
  REG_F(regs) |= ((REG_A(regs) & (1 << 3)) & (value & (1 << 3))) ? ZB_FLAG_H : 0;
  REG_F(regs) |= ((REG_A(regs) & (1 << 7)) & (value & (1 << 7))) ? ZB_FLAG_C : 0;

  REG_A(regs) += value;
  REG_F(regs) |= (REG_A(regs) == 0 ? ZB_FLAG_Z : 0);
  return 8;
}

u_int8_t add_sp_r8(struct zb_bus * bus, struct zb_registers * regs)
{
  zb_sbyte operand;

  operand = bus_read8_signed(bus, REG_PC(regs) + 1);
  REG_F(regs) = 0x0;
  REG_F(regs) |= ((REG_SP(regs) & 0xF) + (operand & 0xF)) == 0x10 ? ZB_FLAG_H : 0;
  REG_F(regs) |= ((int)REG_SP(regs) + operand > 0xFFFF) ? ZB_FLAG_C : 0;
  REG_SP(regs) += operand;
  return 16;
}


#define func_adc_a_r8(rname) \
  bool carry; \
 \
  carry = (REG_F(regs) | ZB_FLAG_C) ? 1 : 0; \
  REG_F(regs) = 0; \
  REG_F(regs) |= ((u_int8_t)(REG_A(regs) + rname(regs) + carry) == 0) ? ZB_FLAG_Z : 0; \
  REG_F(regs) |= ((REG_A(regs) & 0xF) + (rname(regs) & 0xF) + carry > 0x0F) ? ZB_FLAG_H : 0; \
  REG_F(regs) |= (((u_int16_t) REG_A(regs)) + ((u_int16_t) rname(regs)) + carry > 0xFF) ? ZB_FLAG_C : 0; \
  REG_A(regs) += rname(regs) + carry; \
  return 4;

u_int8_t adc_a_a(struct zb_bus * bus, struct zb_registers * regs) { func_adc_a_r8(REG_A) }
u_int8_t adc_a_b(struct zb_bus * bus, struct zb_registers * regs) { func_adc_a_r8(REG_B) }
u_int8_t adc_a_c(struct zb_bus * bus, struct zb_registers * regs) { func_adc_a_r8(REG_C) }
u_int8_t adc_a_d(struct zb_bus * bus, struct zb_registers * regs) { func_adc_a_r8(REG_D) }
u_int8_t adc_a_e(struct zb_bus * bus, struct zb_registers * regs) { func_adc_a_r8(REG_E) }
u_int8_t adc_a_h(struct zb_bus * bus, struct zb_registers * regs) { func_adc_a_r8(REG_H) }
u_int8_t adc_a_l(struct zb_bus * bus, struct zb_registers * regs) { func_adc_a_r8(REG_L) }

u_int8_t adc_a_hl(struct zb_bus * bus, struct zb_registers * regs)
{
  bool carry;
  zb_byte value;

  carry = (REG_F(regs) | ZB_FLAG_C) ? 1 : 0;
  value = bus_read8(bus, REG_HL(regs));

  REG_F(regs) = 0;
  REG_F(regs) |= ((u_int8_t)(REG_A(regs) + value + carry) == 0) ? ZB_FLAG_Z : 0;
  REG_F(regs) |= ((REG_A(regs) & 0xF) + (value & 0xF) + carry > 0x0F) ? ZB_FLAG_H : 0;
  REG_F(regs) |= (((u_int16_t) REG_A(regs)) + ((u_int16_t) value) + carry > 0xFF) ? ZB_FLAG_C : 0;
  REG_A(regs) += value + carry;

  return 8;
}

u_int8_t adc_a_d8(struct zb_bus * bus, struct zb_registers * regs)
{
  bool carry;
  zb_byte value;

  carry = (REG_F(regs) | ZB_FLAG_C) ? 1 : 0;
  value = bus_read8(bus, REG_PC(regs) + 1);

  REG_F(regs) = 0;
  REG_F(regs) |= ((u_int8_t)(REG_A(regs) + value + carry) == 0) ? ZB_FLAG_Z : 0;
  REG_F(regs) |= ((REG_A(regs) & 0xF) + (value & 0xF) + carry > 0x0F) ? ZB_FLAG_H : 0;
  REG_F(regs) |= (((u_int16_t) REG_A(regs)) + ((u_int16_t) value) + carry > 0xFF) ? ZB_FLAG_C : 0;
  REG_A(regs) += value + carry;

  return 8;
}

#define func_sub_a_r8(rname) \
  REG_F(regs) = 0 | ZB_FLAG_N;                                          \
  REG_F(regs) |= (REG_A(regs) == rname(regs)) ? ZB_FLAG_Z : 0;          \
  REG_F(regs) |= ((REG_A(regs) & (1 << 3)) < (rname(regs) & (1 << 3))) ? ZB_FLAG_H : 0; \
  REG_F(regs) |= (REG_A(regs) < rname(regs)) ? ZB_FLAG_C : 0;           \
  REG_A(regs) -= rname(regs);                                           \
  return 4;

u_int8_t sub_a_a(struct zb_bus * bus, struct zb_registers * regs) { func_sub_a_r8(REG_A) }
u_int8_t sub_a_b(struct zb_bus * bus, struct zb_registers * regs) { func_sub_a_r8(REG_A) }
u_int8_t sub_a_c(struct zb_bus * bus, struct zb_registers * regs) { func_sub_a_r8(REG_A) }
u_int8_t sub_a_d(struct zb_bus * bus, struct zb_registers * regs) { func_sub_a_r8(REG_A) }
u_int8_t sub_a_e(struct zb_bus * bus, struct zb_registers * regs) { func_sub_a_r8(REG_A) }
u_int8_t sub_a_h(struct zb_bus * bus, struct zb_registers * regs) { func_sub_a_r8(REG_A) }
u_int8_t sub_a_l(struct zb_bus * bus, struct zb_registers * regs) { func_sub_a_r8(REG_A) }

u_int8_t sub_a_hl(struct zb_bus * bus, struct zb_registers * regs)
{
  zb_byte value;

  value = bus_read8(bus, REG_HL(regs));

  REG_F(regs) = 0 | ZB_FLAG_N;
  REG_F(regs) |= (REG_A(regs) == value) ? ZB_FLAG_Z : 0;
  REG_F(regs) |= ((REG_A(regs) & (1 << 3)) < (value & (1 << 3))) ? ZB_FLAG_H : 0;
  REG_F(regs) |= (REG_A(regs) < value) ? ZB_FLAG_C : 0;
  REG_A(regs) -= value;
  return 8;
}

u_int8_t sub_a_d8(struct zb_bus * bus, struct zb_registers * regs)
{
  zb_byte value;

  value = bus_read8(bus, REG_PC(regs) + 1);

  REG_F(regs) = 0 | ZB_FLAG_N;
  REG_F(regs) |= (REG_A(regs) == value) ? ZB_FLAG_Z : 0;
  REG_F(regs) |= ((REG_A(regs) & (1 << 3)) < (value & (1 << 3))) ? ZB_FLAG_H : 0;
  REG_F(regs) |= (REG_A(regs) < value) ? ZB_FLAG_C : 0;
  REG_A(regs) -= value;
  return 8;
}

#define func_sbc_a_r8(rname)                                            \
  bool carry;                                                           \
                                                                        \
  carry = (REG_F(regs) | ZB_FLAG_C) ? 1 : 0;                            \
  REG_F(regs) = 0 | ZB_FLAG_N;                                          \
  REG_F(regs) |= (REG_A(regs) - rname(regs) - carry) ? ZB_FLAG_Z : 0;   \
  REG_F(regs) |= ((REG_A(regs) & (1 << 3)) < (rname(regs) & (1 << 3)) + carry) ? ZB_FLAG_H : 0; \
  REG_F(regs) |= (REG_A(regs) < rname(regs) + carry) ? ZB_FLAG_C : 0;   \
  REG_A(regs) -= rname(regs);                                           \
  return 4;

u_int8_t sbc_a_a(struct zb_bus * bus, struct zb_registers * regs) { func_sbc_a_r8(REG_A) }
u_int8_t sbc_a_b(struct zb_bus * bus, struct zb_registers * regs) { func_sbc_a_r8(REG_A) }
u_int8_t sbc_a_c(struct zb_bus * bus, struct zb_registers * regs) { func_sbc_a_r8(REG_A) }
u_int8_t sbc_a_d(struct zb_bus * bus, struct zb_registers * regs) { func_sbc_a_r8(REG_A) }
u_int8_t sbc_a_e(struct zb_bus * bus, struct zb_registers * regs) { func_sbc_a_r8(REG_A) }
u_int8_t sbc_a_h(struct zb_bus * bus, struct zb_registers * regs) { func_sbc_a_r8(REG_A) }
u_int8_t sbc_a_l(struct zb_bus * bus, struct zb_registers * regs) { func_sbc_a_r8(REG_A) }

u_int8_t sbc_a_hl(struct zb_bus * bus, struct zb_registers * regs)
{
  zb_byte value;
  bool carry;

  value = bus_read8(bus, REG_HL(regs));
  carry = (REG_F(regs) | ZB_FLAG_C) ? 1 : 0;
  REG_F(regs) = 0 | ZB_FLAG_N;
  REG_F(regs) |= (REG_A(regs) - value - carry) ? ZB_FLAG_Z : 0;
  REG_F(regs) |= ((REG_A(regs) & (1 << 3)) < (value & (1 << 3)) + carry) ? ZB_FLAG_H : 0;
  REG_F(regs) |= (REG_A(regs) < value + carry) ? ZB_FLAG_C : 0;
  REG_A(regs) -= value;
  return 8;
}

u_int8_t sbc_a_d8(struct zb_bus * bus, struct zb_registers * regs)
{
  zb_byte value;
  bool carry;

  value = bus_read8(bus, REG_PC(regs) + 1);
  carry = (REG_F(regs) | ZB_FLAG_C) ? 1 : 0;
  REG_F(regs) = 0 | ZB_FLAG_N;
  REG_F(regs) |= (REG_A(regs) - value - carry) ? ZB_FLAG_Z : 0;
  REG_F(regs) |= ((REG_A(regs) & (1 << 3)) < (value & (1 << 3)) + carry) ? ZB_FLAG_H : 0;
  REG_F(regs) |= (REG_A(regs) < value + carry) ? ZB_FLAG_C : 0;
  REG_A(regs) -= value;
  return 8;
}

u_int8_t daa(struct zb_bus * bus, struct zb_registers * regs)
{
  /* based on https://ehaskins.com/2018-01-30%20Z80%20DAA/ */
  zb_byte correction = 0;
  bool set_carry = false;

  if (HAS_FLAG_H(regs) || (!HAS_FLAG_N(regs) && (REG_A(regs) & 0xf) > 9))
    correction |= 0x06;

  if (HAS_FLAG_C(regs) || (!HAS_FLAG_N(regs) && REG_A(regs) > 0x99)) {
    correction |= 0x60;
    set_carry = true;
  }

  REG_A(regs) += HAS_FLAG_N(regs) ? -correction : correction;

  REG_F(regs) = 0 \
    | (HAS_FLAG_N(regs) ? ZB_FLAG_N : 0) \
    | (REG_A(regs) == 0 ? ZB_FLAG_Z : 0) \
    | (set_carry ? ZB_FLAG_C : 0);
  return 4;
}

u_int8_t cpl(struct zb_bus * bus, struct zb_registers * regs)
{
  REG_F(regs) = 0 | ZB_FLAG_N | ZB_FLAG_H;
  REG_A(regs) ^= 0xFF;
  return 4;
}
