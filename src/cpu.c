#include <sys/types.h>
#include <strings.h>
#include <stdio.h>

#include "bus.h"
#include "cpu.h"
#include "opcode.h"
#include "logging.h"
#include "zameboy.h"

#define INSTR_DUMP_BUF_SIZE 16

void format_instrnuction_dump(char * buf, const zb_byte * addr, size_t len)
{
  bzero(buf, len);

  switch (len) {
  case 1:
    snprintf(buf, INSTR_DUMP_BUF_SIZE, "0x%02x .... ....", *addr);
    break;
  case 2:
    snprintf(buf, INSTR_DUMP_BUF_SIZE, "0x%02x 0x%02x ....", *addr, *(addr + 1));
    break;
  case 3:
    snprintf(buf, INSTR_DUMP_BUF_SIZE, "0x%02x 0x%02x 0x%02x", *addr, *(addr + 1), *(addr + 2));
    break;
  default:
    snprintf(buf, INSTR_DUMP_BUF_SIZE, "wrong size");
    break;
  }
}

u_int64_t run(u_int64_t cycles_left, struct zameboy * zb)
{
  u_int8_t consumed;
  const struct instr_def * instr;
  char dump_buf[INSTR_DUMP_BUF_SIZE];

  log_debug(LOG_CPU, "About to run %lu cpu cycles.", cycles_left);

  while (cycles_left > 0) {
    if (zb->registers.pc == 0x100 && !zb->bus.boot_over) {
      zb->bus.boot_over = true;
      log_debug(LOG_CPU, "Boot sequence over");
    }
    /* deal with the HALT and STOP status somewhere here */

    instr = fetch(&zb->bus, zb->registers.pc);
    if (!instr->implem) {
      log_warning(LOG_CPU, "[pc: %#06x]Instruction (%s) not implemented; skipping.", zb->registers.pc, instr->mnemonic);
      zb->registers.pc += instr->length;
      cycles_left -= instr->max_cycles;
      continue;
    }
    else if (instr->max_cycles > cycles_left) {
      log_debug(LOG_CPU, "Not enough cycles left to execute (%s).", instr->mnemonic);
      break;
    }

    format_instrnuction_dump(dump_buf, bus_get_buffer(&zb->bus, zb->registers.pc), instr->length);
    log_debug(LOG_CPU, "[pc: %#06x] Executing instruction [%s] %s", zb->registers.pc, dump_buf, instr->mnemonic);
    consumed = (*instr->implem)(&zb->bus, &zb->registers);
    zb->registers.pc += instr->length;
    cycles_left -= consumed;
  }

  return cycles_left;
}
