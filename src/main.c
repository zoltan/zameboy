#include <sys/errno.h>
#include <sys/param.h>
#include <sys/types.h>
#include <fcntl.h>
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include "cpu.h"
#include "logging.h"
#include "zameboy.h"

static void usage() {
  puts("\n\tUsage:");
  puts("\tzameboy [-v] [-h] [-p address] path/to/rom");
  puts("\t\t-v: increase verbosity, repeat to increase even more");
  puts("\t\t-h: print this help");
  puts("\t\t-p: initial value for register PC");
  puts("");
}

static void get_next_frame(u_int64_t frame_num, struct timespec * restrict start,  struct timespec * restrict next)
{
    /* 1 frame every 16742005 ns (10^9 / 59.73) */
    next->tv_sec = start->tv_sec + frame_num * 16742005 / 1e9;
    next->tv_nsec = start->tv_nsec + frame_num * 16742005 % (int)1e9;
    if (next->tv_nsec > 1e9) {
      next->tv_sec++;
      next->tv_nsec -= 1e9;
    }
}

static void _load_rom(int fd, zb_byte * dest, ssize_t size)
{
  ssize_t ret, offset = 0;

  while (size > 0) {
    ret = read(fd, dest + offset, MIN(size, 4096));
    if (ret == -1) {
      log_error(LOG_CORE, "Error reading rom: %s", strerror(errno));
      exit(-1);
    }

    offset += ret;
    size -= ret;

    if (ret == 0 && size > 0) {
      log_error(LOG_CORE, "Unexpected end of file while reading ROM. Missing %zd bytes.", size);
      exit(-1);
    }
  }
}

static void load_cartridge(struct zb_bus * bus, const char * path)
{
  int       fd;

  fd = open(path, O_RDONLY);
  if (fd == -1) {
    log_error(LOG_CORE, "Can't open file \"%s\": %s", path, strerror(errno));
    exit(1);
  }

  log_debug(LOG_CORE, "Loading ROM0.");
  _load_rom(fd, bus->rom_0, 0x4000);
  log_debug(LOG_CORE, "Loading ROM1.");
  _load_rom(fd, bus->rom_1, 0x4000);

  close(fd);
}

static void init(struct zameboy * zb, zb_addr pc_init, const char * rom_path)
{
  zb_byte boot_seq[] =
    {

      0x31, 0xfe, 0xff, 0xaf, 0x21, 0xff, 0x9f, 0x32, 0xcb, 0x7c, 0x20, 0xfb, 0x21, 0x26, 0xff, 0x0e,
      0x11, 0x3e, 0x80, 0x32, 0xe2, 0x0c, 0x3e, 0xf3, 0xe2, 0x32, 0x3e, 0x77, 0x77, 0x3e, 0xfc, 0xe0,
      0x47, 0x11, 0x04, 0x01, 0x21, 0x10, 0x80, 0x1a, 0xcd, 0x95, 0x00, 0xcd, 0x96, 0x00, 0x13, 0x7b,
      0xfe, 0x34, 0x20, 0xf3, 0x11, 0xd8, 0x00, 0x06, 0x08, 0x1a, 0x13, 0x22, 0x23, 0x05, 0x20, 0xf9,
      0x3e, 0x19, 0xea, 0x10, 0x99, 0x21, 0x2f, 0x99, 0x0e, 0x0c, 0x3d, 0x28, 0x08, 0x32, 0x0d, 0x20,
      0xf9, 0x2e, 0x0f, 0x18, 0xf3, 0x67, 0x3e, 0x64, 0x57, 0xe0, 0x42, 0x3e, 0x91, 0xe0, 0x40, 0x04,
      0x1e, 0x02, 0x0e, 0x0c, 0xf0, 0x44, 0xfe, 0x90, 0x20, 0xfa, 0x0d, 0x20, 0xf7, 0x1d, 0x20, 0xf2,
      0x0e, 0x13, 0x24, 0x7c, 0x1e, 0x83, 0xfe, 0x62, 0x28, 0x06, 0x1e, 0xc1, 0xfe, 0x64, 0x20, 0x06,
      0x7b, 0xe2, 0x0c, 0x3e, 0x87, 0xe2, 0xf0, 0x42, 0x90, 0xe0, 0x42, 0x15, 0x20, 0xd2, 0x05, 0x20,
      0x4f, 0x16, 0x20, 0x18, 0xcb, 0x4f, 0x06, 0x04, 0xc5, 0xcb, 0x11, 0x17, 0xc1, 0xcb, 0x11, 0x17,
      0x05, 0x20, 0xf5, 0x22, 0x23, 0x22, 0x23, 0xc9, 0xce, 0xed, 0x66, 0x66, 0xcc, 0x0d, 0x00, 0x0b,
      0x03, 0x73, 0x00, 0x83, 0x00, 0x0c, 0x00, 0x0d, 0x00, 0x08, 0x11, 0x1f, 0x88, 0x89, 0x00, 0x0e,
      0xdc, 0xcc, 0x6e, 0xe6, 0xdd, 0xdd, 0xd9, 0x99, 0xbb, 0xbb, 0x67, 0x63, 0x6e, 0x0e, 0xec, 0xcc,
      0xdd, 0xdc, 0x99, 0x9f, 0xbb, 0xb9, 0x33, 0x3e, 0x3c, 0x42, 0xb9, 0xa5, 0xb9, 0xa5, 0x42, 0x3c,
      0x21, 0x04, 0x01, 0x11, 0xa8, 0x00, 0x1a, 0x13, 0xbe, 0x20, 0xfe, 0x23, 0x7d, 0xfe, 0x34, 0x20,
      0xf5, 0x06, 0x19, 0x78, 0x86, 0x23, 0x05, 0x20, 0xfb, 0x86, 0x20, 0xfe, 0x3e, 0x01, 0xe0, 0x50,

    };

  zb->bus.boot_over = false;
  zb->bus.ime = false;
  zb->bus.halted = false;
  zb->bus.stopped = false;
  zb->registers.pc = pc_init;
  memcpy(zb->bus.boot_rom, boot_seq, sizeof(boot_seq));
  load_cartridge(&zb->bus, rom_path);
  if (zb->bus.rom_0[0x147] != 0) {
    log_error(LOG_CORE, "MBC detected [%#x], cartridge not supported.", zb->bus.rom_0[0x147]);
    exit(1);
  }
}

void core_loop(zb_addr pc_init, const char * rom_path)
{
  struct timespec next_frame, start;
  u_int64_t frame_num = 0;
  u_int64_t cycles = 0;
  struct zameboy zameboy;

  init(&zameboy, pc_init, rom_path);
  if (pc_init)
    log_info(LOG_CORE, "Starting with PC at 0x%04x", pc_init);

  if (clock_gettime(CLOCK_MONOTONIC_PRECISE, &start) != 0) {
    perror("clock_gettime");
    return;
  }

  while (1) {

    /* one frame covers 17555 cycles: 1024^2 / 59.73 = 17555.265 */
    cycles = run(17555 + cycles, &zameboy);

    frame_num++;
    get_next_frame(frame_num, &start, &next_frame);
    log_debug(LOG_CORE, "next frame (num %lu) at %lu.%lu",
              frame_num, next_frame.tv_sec, next_frame.tv_nsec);
    clock_nanosleep(CLOCK_MONOTONIC_FAST, TIMER_ABSTIME, &next_frame, NULL);

    if (zameboy.bus.boot_over)
      break;
  }
}

static zb_addr read_addr(const char * arg)
{
  char * end;
  unsigned long value;

  value = strtoull(arg, &end, 0);
  if (*end) {
    fprintf(stderr, "Can't parse value '%s' as a 16bits address.\n", arg);
    exit(1);
  }
  else if (value > 0xFFFF) {
    fprintf(stderr, "Value %lu is too large for a 16bits address\n", value);
    exit(1);
  }
  return value;
}


int main(int argc, char *argv[])
{
  int ch, verbosity = 1;
  zb_addr pc_init = 0;

  while ((ch = getopt(argc, argv, "hvp:")) != -1) {
    switch (ch) {
    case 'v':
      ++verbosity;
      break;
    case 'p':
      pc_init = read_addr(optarg);
      break;
    case 'h':
    default:
      usage();
      return -1;
    }
  }
  argc -= optind;
  argv += optind;

  logging_set_level(verbosity);
  log_info(LOG_CORE, "ZameBoy");

  if (argc == 0) {
    log_error(LOG_CORE, "Missing rom to load.");
    return 1;
  }

  core_loop(pc_init, argv[0]);
  return 0;
}
