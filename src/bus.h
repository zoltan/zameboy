#ifndef BUS_H
#define BUS_H

#include "zameboy.h"

zb_byte     bus_read8(const struct zb_bus * bus, zb_addr addr);
zb_sbyte    bus_read8_signed(const struct zb_bus * bus, zb_addr addr);
u_int16_t   bus_read16(const struct zb_bus * bus, zb_addr addr);

const zb_byte * bus_get_buffer(const struct zb_bus * bus, zb_addr addr);

void bus_write8(struct zb_bus * bus, zb_addr addr, zb_byte value);
void bus_write16(struct zb_bus * bus, zb_addr addr, zb_dbyte value);

#endif /* BUS_H */
