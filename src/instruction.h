#ifndef ZB_INSTRUCTION_H
#define ZB_INSTRUCTION_H

#include "zameboy.h"
#include <sys/types.h>

/* instr. misc */
u_int8_t noop(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t di(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t ei(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t halt(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t stop(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t scf(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t ccf(struct zb_bus * bus, struct zb_registers * regs);

/* instr load */
u_int8_t load_bc_d16(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_de_d16(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_hl_d16(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_sp_d16(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_sp_hl(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_a16_sp(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_hl_sp_r8(struct zb_bus * bus, struct zb_registers * regs);

u_int8_t load_bc_a(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_de_a(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_hlplus_a(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_hlminus_a(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_a_bc(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_a_de(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_a_hlplus(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_a_hlminus(struct zb_bus * bus, struct zb_registers * regs);

u_int8_t load_a_high_c(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_high_c_a(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_a_high_a8(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_high_a8_a(struct zb_bus * bus, struct zb_registers * regs);

u_int8_t load_a_d8(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_b_d8(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_c_d8(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_d_d8(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_e_d8(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_h_d8(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_l_d8(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_hl_d8(struct zb_bus * bus, struct zb_registers * regs);

u_int8_t load_hl_a(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_hl_b(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_hl_c(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_hl_d(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_hl_e(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_hl_h(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_hl_l(struct zb_bus * bus, struct zb_registers * regs);

u_int8_t load_a_a(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_a_b(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_a_c(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_a_d(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_a_e(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_a_h(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_a_l(struct zb_bus * bus, struct zb_registers * regs);

u_int8_t load_b_a(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_b_b(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_b_c(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_b_d(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_b_e(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_b_h(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_b_l(struct zb_bus * bus, struct zb_registers * regs);

u_int8_t load_c_a(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_c_b(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_c_c(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_c_d(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_c_e(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_c_h(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_c_l(struct zb_bus * bus, struct zb_registers * regs);

u_int8_t load_d_a(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_d_b(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_d_c(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_d_d(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_d_e(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_d_h(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_d_l(struct zb_bus * bus, struct zb_registers * regs);

u_int8_t load_e_a(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_e_b(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_e_c(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_e_d(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_e_e(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_e_h(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_e_l(struct zb_bus * bus, struct zb_registers * regs);

u_int8_t load_h_a(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_h_b(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_h_c(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_h_d(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_h_e(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_h_h(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_h_l(struct zb_bus * bus, struct zb_registers * regs);

u_int8_t load_l_a(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_l_b(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_l_c(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_l_d(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_l_e(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_l_h(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_l_l(struct zb_bus * bus, struct zb_registers * regs);

u_int8_t load_a_hl(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_b_hl(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_c_hl(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_d_hl(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_e_hl(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_h_hl(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_l_hl(struct zb_bus * bus, struct zb_registers * regs);

u_int8_t load_a16_a(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t load_a_a16(struct zb_bus * bus, struct zb_registers * regs);


u_int8_t push_bc(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t push_de(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t push_hl(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t push_af(struct zb_bus * bus, struct zb_registers * regs);

u_int8_t pop_bc(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t pop_de(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t pop_hl(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t pop_af(struct zb_bus * bus, struct zb_registers * regs);

/* instr logic */
u_int8_t and_a(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t and_b(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t and_c(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t and_d(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t and_e(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t and_h(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t and_l(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t and_hl(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t and_d8(struct zb_bus * bus, struct zb_registers * regs);

u_int8_t xor_a(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t xor_b(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t xor_c(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t xor_d(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t xor_e(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t xor_h(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t xor_l(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t xor_hl(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t xor_d8(struct zb_bus * bus, struct zb_registers * regs);

u_int8_t or_a(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t or_b(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t or_c(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t or_d(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t or_e(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t or_h(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t or_l(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t or_hl(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t or_d8(struct zb_bus * bus, struct zb_registers * regs);

u_int8_t cp_a(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t cp_b(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t cp_c(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t cp_d(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t cp_e(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t cp_h(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t cp_l(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t cp_hl(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t cp_d8(struct zb_bus * bus, struct zb_registers * regs);

u_int8_t inc8_a(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t inc8_b(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t inc8_c(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t inc8_d(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t inc8_e(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t inc8_h(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t inc8_l(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t inc8_hl(struct zb_bus * bus, struct zb_registers * regs);

u_int8_t dec8_a(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t dec8_b(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t dec8_c(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t dec8_d(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t dec8_e(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t dec8_h(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t dec8_l(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t dec8_hl(struct zb_bus * bus, struct zb_registers * regs);

u_int8_t inc16_bc(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t inc16_de(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t inc16_hl(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t inc16_sp(struct zb_bus * bus, struct zb_registers * regs);

u_int8_t dec16_bc(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t dec16_de(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t dec16_hl(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t dec16_sp(struct zb_bus * bus, struct zb_registers * regs);

u_int8_t add_hl_bc(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t add_hl_de(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t add_hl_hl(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t add_hl_sp(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t add_sp_r8(struct zb_bus * bus, struct zb_registers * regs);

u_int8_t add_a_a(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t add_a_b(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t add_a_c(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t add_a_d(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t add_a_e(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t add_a_h(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t add_a_l(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t add_a_hl(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t add_a_d8(struct zb_bus * bus, struct zb_registers * regs);

u_int8_t adc_a_a(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t adc_a_b(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t adc_a_c(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t adc_a_d(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t adc_a_e(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t adc_a_h(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t adc_a_l(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t adc_a_hl(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t adc_a_d8(struct zb_bus * bus, struct zb_registers * regs);

u_int8_t sub_a_a(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t sub_a_b(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t sub_a_c(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t sub_a_d(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t sub_a_e(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t sub_a_h(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t sub_a_l(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t sub_a_hl(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t sub_a_d8(struct zb_bus * bus, struct zb_registers * regs);

u_int8_t sbc_a_a(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t sbc_a_b(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t sbc_a_c(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t sbc_a_d(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t sbc_a_e(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t sbc_a_h(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t sbc_a_l(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t sbc_a_hl(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t sbc_a_d8(struct zb_bus * bus, struct zb_registers * regs);

u_int8_t daa(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t cpl(struct zb_bus * bus, struct zb_registers * regs);


/* instr jump */
u_int8_t jr_r8(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t jr_c_r8(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t jr_z_r8(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t jr_nc_r8(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t jr_nz_r8(struct zb_bus * bus, struct zb_registers * regs);

u_int8_t jp_a16(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t jp_c_a16(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t jp_z_a16(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t jp_nc_a16(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t jp_nz_a16(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t jp_hl(struct zb_bus * bus, struct zb_registers * regs);

u_int8_t call(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t call_c(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t call_z(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t call_nc(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t call_nz(struct zb_bus * bus, struct zb_registers * regs);

u_int8_t ret(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t ret_c(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t ret_z(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t ret_nc(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t ret_nz(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t reti(struct zb_bus * bus, struct zb_registers * regs);

u_int8_t rst_00h(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t rst_08h(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t rst_10h(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t rst_18h(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t rst_20h(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t rst_28h(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t rst_30h(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t rst_38h(struct zb_bus * bus, struct zb_registers * regs);


/* instr single bit */
u_int8_t bit0_a(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t bit0_b(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t bit0_c(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t bit0_d(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t bit0_e(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t bit0_h(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t bit0_l(struct zb_bus * bus, struct zb_registers * regs);

u_int8_t bit1_a(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t bit1_b(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t bit1_c(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t bit1_d(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t bit1_e(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t bit1_h(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t bit1_l(struct zb_bus * bus, struct zb_registers * regs);

u_int8_t bit2_a(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t bit2_b(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t bit2_c(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t bit2_d(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t bit2_e(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t bit2_h(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t bit2_l(struct zb_bus * bus, struct zb_registers * regs);

u_int8_t bit3_a(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t bit3_b(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t bit3_c(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t bit3_d(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t bit3_e(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t bit3_h(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t bit3_l(struct zb_bus * bus, struct zb_registers * regs);

u_int8_t bit4_a(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t bit4_b(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t bit4_c(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t bit4_d(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t bit4_e(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t bit4_h(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t bit4_l(struct zb_bus * bus, struct zb_registers * regs);

u_int8_t bit5_a(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t bit5_b(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t bit5_c(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t bit5_d(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t bit5_e(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t bit5_h(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t bit5_l(struct zb_bus * bus, struct zb_registers * regs);

u_int8_t bit6_a(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t bit6_b(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t bit6_c(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t bit6_d(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t bit6_e(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t bit6_h(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t bit6_l(struct zb_bus * bus, struct zb_registers * regs);

u_int8_t bit7_a(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t bit7_b(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t bit7_c(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t bit7_d(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t bit7_e(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t bit7_h(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t bit7_l(struct zb_bus * bus, struct zb_registers * regs);

u_int8_t bit0_hl(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t bit1_hl(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t bit2_hl(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t bit3_hl(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t bit4_hl(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t bit5_hl(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t bit6_hl(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t bit7_hl(struct zb_bus * bus, struct zb_registers * regs);

u_int8_t rra(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t rrca(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t rla(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t rlca(struct zb_bus * bus, struct zb_registers * regs);


u_int8_t rlc_a(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t rlc_b(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t rlc_c(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t rlc_d(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t rlc_e(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t rlc_h(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t rlc_l(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t rlc_hl(struct zb_bus * bus, struct zb_registers * regs);

u_int8_t rrc_a(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t rrc_b(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t rrc_c(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t rrc_d(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t rrc_e(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t rrc_h(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t rrc_l(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t rrc_hl(struct zb_bus * bus, struct zb_registers * regs);

u_int8_t rl_a(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t rl_b(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t rl_c(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t rl_d(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t rl_e(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t rl_h(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t rl_l(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t rl_hl(struct zb_bus * bus, struct zb_registers * regs);

u_int8_t rr_a(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t rr_b(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t rr_c(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t rr_d(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t rr_e(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t rr_h(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t rr_l(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t rr_hl(struct zb_bus * bus, struct zb_registers * regs);

u_int8_t sla_a(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t sla_b(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t sla_c(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t sla_d(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t sla_e(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t sla_h(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t sla_l(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t sla_hl(struct zb_bus * bus, struct zb_registers * regs);

u_int8_t sra_a(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t sra_b(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t sra_c(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t sra_d(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t sra_e(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t sra_h(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t sra_l(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t sra_hl(struct zb_bus * bus, struct zb_registers * regs);

u_int8_t srl_a(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t srl_b(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t srl_c(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t srl_d(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t srl_e(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t srl_h(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t srl_l(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t srl_hl(struct zb_bus * bus, struct zb_registers * regs);

u_int8_t swap_a(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t swap_b(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t swap_c(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t swap_d(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t swap_e(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t swap_h(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t swap_l(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t swap_hl(struct zb_bus * bus, struct zb_registers * regs);

u_int8_t reset_0_a(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t reset_0_b(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t reset_0_c(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t reset_0_d(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t reset_0_e(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t reset_0_h(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t reset_0_l(struct zb_bus * bus, struct zb_registers * regs);

u_int8_t reset_1_a(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t reset_1_b(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t reset_1_c(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t reset_1_d(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t reset_1_e(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t reset_1_h(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t reset_1_l(struct zb_bus * bus, struct zb_registers * regs);

u_int8_t reset_2_a(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t reset_2_b(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t reset_2_c(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t reset_2_d(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t reset_2_e(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t reset_2_h(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t reset_2_l(struct zb_bus * bus, struct zb_registers * regs);

u_int8_t reset_3_a(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t reset_3_b(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t reset_3_c(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t reset_3_d(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t reset_3_e(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t reset_3_h(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t reset_3_l(struct zb_bus * bus, struct zb_registers * regs);

u_int8_t reset_4_a(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t reset_4_b(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t reset_4_c(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t reset_4_d(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t reset_4_e(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t reset_4_h(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t reset_4_l(struct zb_bus * bus, struct zb_registers * regs);

u_int8_t reset_5_a(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t reset_5_b(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t reset_5_c(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t reset_5_d(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t reset_5_e(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t reset_5_h(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t reset_5_l(struct zb_bus * bus, struct zb_registers * regs);

u_int8_t reset_6_a(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t reset_6_b(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t reset_6_c(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t reset_6_d(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t reset_6_e(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t reset_6_h(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t reset_6_l(struct zb_bus * bus, struct zb_registers * regs);

u_int8_t reset_7_a(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t reset_7_b(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t reset_7_c(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t reset_7_d(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t reset_7_e(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t reset_7_h(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t reset_7_l(struct zb_bus * bus, struct zb_registers * regs);

u_int8_t reset_0_hl(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t reset_1_hl(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t reset_2_hl(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t reset_3_hl(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t reset_4_hl(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t reset_5_hl(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t reset_6_hl(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t reset_7_hl(struct zb_bus * bus, struct zb_registers * regs);

u_int8_t set_0_a(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t set_0_b(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t set_0_c(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t set_0_d(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t set_0_e(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t set_0_h(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t set_0_l(struct zb_bus * bus, struct zb_registers * regs);

u_int8_t set_1_a(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t set_1_b(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t set_1_c(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t set_1_d(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t set_1_e(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t set_1_h(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t set_1_l(struct zb_bus * bus, struct zb_registers * regs);

u_int8_t set_2_a(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t set_2_b(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t set_2_c(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t set_2_d(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t set_2_e(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t set_2_h(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t set_2_l(struct zb_bus * bus, struct zb_registers * regs);

u_int8_t set_3_a(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t set_3_b(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t set_3_c(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t set_3_d(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t set_3_e(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t set_3_h(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t set_3_l(struct zb_bus * bus, struct zb_registers * regs);

u_int8_t set_4_a(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t set_4_b(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t set_4_c(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t set_4_d(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t set_4_e(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t set_4_h(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t set_4_l(struct zb_bus * bus, struct zb_registers * regs);

u_int8_t set_5_a(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t set_5_b(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t set_5_c(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t set_5_d(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t set_5_e(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t set_5_h(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t set_5_l(struct zb_bus * bus, struct zb_registers * regs);

u_int8_t set_6_a(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t set_6_b(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t set_6_c(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t set_6_d(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t set_6_e(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t set_6_h(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t set_6_l(struct zb_bus * bus, struct zb_registers * regs);

u_int8_t set_7_a(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t set_7_b(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t set_7_c(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t set_7_d(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t set_7_e(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t set_7_h(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t set_7_l(struct zb_bus * bus, struct zb_registers * regs);

u_int8_t set_0_hl(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t set_1_hl(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t set_2_hl(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t set_3_hl(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t set_4_hl(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t set_5_hl(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t set_6_hl(struct zb_bus * bus, struct zb_registers * regs);
u_int8_t set_7_hl(struct zb_bus * bus, struct zb_registers * regs);

#endif /* ZB_INSTRUCTION_H */
