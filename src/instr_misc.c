#include "bus.h"
#include "instruction.h"
#include "zameboy.h"


u_int8_t noop(struct zb_bus * bus, struct zb_registers * regs)
{
  return 4;
}

u_int8_t di(struct zb_bus * bus, struct zb_registers * regs)
{
  bus->ime = false;
  return 4;
}

u_int8_t ei(struct zb_bus * bus, struct zb_registers * regs)
{
  bus->ime = true;
  return 4;
}

u_int8_t halt(struct zb_bus * bus, struct zb_registers * regs)
{
  bus->halted = true;
  return 4;
}

u_int8_t stop(struct zb_bus * bus, struct zb_registers * regs)
{
  bus->stopped = true;
  return 4;
}

u_int8_t scf(struct zb_bus * bus, struct zb_registers * regs)
{
  REG_F(regs) = 0x0 | (REG_F(regs) & ZB_FLAG_Z) | ZB_FLAG_C;
  return 4;
}

u_int8_t ccf(struct zb_bus * bus, struct zb_registers * regs)
{
  REG_F(regs) = 0x0 | (REG_F(regs) & ZB_FLAG_Z) | (REG_F(regs) ^ ZB_FLAG_C);
  return 4;
}
